# Open Earth View Library

Webpack based boilerplate for producing Open Earth View libraries (Input: ES6, Output: library)

## Features

* Webpack based.
* ES6 as a source.
* ES6 test setup with [Mocha](http://mochajs.org/) and [Chai](http://chaijs.com/).
* Linting with [ESLint](http://eslint.org/).

## Process

```
ES6 source files
       |
       |
    webpack
       |
       +--- babel, eslint
       |
  ready to use
     library
```

## Getting started

1. Build your library
  * Run `npm install` to get the project's dependencies
  * Run `./node_modules/.bin/webpack --config webpack.config.js` to build the project

