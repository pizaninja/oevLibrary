/**
Open Earth View - library
The MIT License (MIT)
Copyright (c) 2017 Clément Igonet

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

* @author Clement Igonet
*/

var toolbox = OpenEarthView.toolbox;
var Logger = OpenEarthView.Logger;

OpenEarthView.Layer.TerrariumTerrain = function(name, urls, options) {
	this.logger = Logger.getInstance('OpenEarthView.Layer.TerrariumTerrainLayer')
  this.name = (name !== undefined) ? name : 'Terrarium';
  if (options !== undefined) {
    this.localData = options.localData;
    this.api_key =(options.api_key !== undefined) ? options.api_key : undefined;
  }
	let url1 = 'https://tile.mapzen.com/mapzen/terrain/v1/normal/${z}/${x}/${y}.png?' +
		(this.api_key !== undefined) ? ('api_key=' + this.api_key) : 'api_key=mapzen-pqfJv5k'
  this.urls = (urls !== undefined) ? urls : [url1];
};

OpenEarthView.Layer.TerrariumTerrain.prototype = {
  constructor: OpenEarthView.Layer.TerrariumTerrain,
  type: 'terrain',
	// logger: Logger.getInstance('OpenEarthView.Layer.TerrariumTerrainLayer'),
  getName: function() {
      return this.name
  },
  getLocalUrl: function(parentTileId) {
		// this.logger.debug('this.localData: ' + JSON.stringify(this.localData));
		if (this.localData === undefined || this.localData === null) {
			return null;
		}
    return this.localData
      .replace('${z}', parentTileId.z)
      .replace('${x}', parentTileId.x)
      .replace('${y}', parentTileId.y);
  },
	getSourceTileId(tileId, zoomRef) {
		let factor = Math.min(OpenEarthView.Layer.TerrariumTerrain.factor - (zoomRef - tileId.z), tileId.z);
		let power = Math.pow(2, factor);
		return {
				z: tileId.z - factor,
				x: Math.floor(tileId.x / power),
				y: Math.floor(tileId.y / power)
		};
	},
  getUrl: function(parentTileId) {
		this.logger.debug('this.urls: ' + JSON.stringify(this.urls));
		if (this.urls === null || this.urls === undefined) {
			return null;
		}
    let scope = this;
		let url;
    // let urls = OpenEarthViewLayers[scope.name];
		let urlRandom = this.urls[
  		Math.floor(Math.random() * this.urls.length)];
		// Process GPS bounds
		minLon = toolbox.tile2long(parentTileId.x, parentTileId.z);
		maxLon = toolbox.tile2long(parentTileId.x + 1, parentTileId.z);
		minLat = toolbox.tile2lat(parentTileId.y + 1, parentTileId.z);
		maxLat = toolbox.tile2lat(parentTileId.y, parentTileId.z);
		url = urlRandom
      .replace('${z}', parentTileId.z)
      .replace('${x}', parentTileId.x)
      .replace('${y}', parentTileId.y);
		// this.logger.debug('url: ' + url);
		return url;
  },
	preProcess: function(tileSupport, tileMesh, tileId, tileShift, tileRef) {
		// console.log('tileId: ' + JSON.stringify(tileId));
		// get heightmap sub-texture
		// let heightmap = toolbox.getSubTexture(parentHeightmap, tileId, factor);
		let factor = Math.min(OpenEarthView.Layer.TerrariumTerrain.factor - (tileRef.z - tileId.z), tileId.z);
		let zoom = tileId.z;
		let xtile = tileId.x;
		let ytile = tileId.y;
		wPeriod = Math.pow(2, 8 - factor);
		hPeriod = Math.pow(2, 8 - factor);

		let trapeze = toolbox.getTrapeze(tileId);
		let height = trapeze.height;
		let width = (trapeze.widthUp + trapeze.widthDown) / 2;
		let widthDelta = (trapeze.widthDown - trapeze.widthUp) / 2;
		tileMesh.geometry = new THREE.PlaneGeometry(width, height, wPeriod - 1, hPeriod - 1);
		tileMesh.geometry.applyMatrix( new THREE.Matrix4().makeTranslation(width / 2, - height / 2, 0) );
		// tileMesh.geometry.computeFaceNormals();
		// tileMesh.geometry.computeVertexNormals();
		// tileMesh.visible = true;
		// tileMesh.geometry.computeTangents();
		tileSupport.position.set(tileShift.x * width, -tileShift.y * height, 0);
	},
	process: function(parentHeightmap, tileSupport, tileMesh, tileId, tileShift, tileRef) {
		// console.log('tileId: ' + JSON.stringify(tileId));
		// get heightmap sub-texture
		// let heightmap = toolbox.getSubTexture(parentHeightmap, tileId, factor);
		let factor = Math.min(OpenEarthView.Layer.TerrariumTerrain.factor - (tileRef.z - tileId.z), tileId.z);
		// 18 -> 13
		// 17 -> 13
		// 16 -> 13

		let zoom = tileId.z;
		let xtile = tileId.x;
		let ytile = tileId.y;

		let trapeze = toolbox.getTrapeze(tileId);
		let height = trapeze.height;
		let width = (trapeze.widthUp + trapeze.widthDown) / 2;
		let widthDelta = (trapeze.widthDown - trapeze.widthUp) / 2;

		this.logger.debug('tileId: ' + JSON.stringify(tileId));
		let period = 1;
		// if (parentHeightmap.image === undefined) {
		// 	this.logger.debug('parentHeightmap.image === undefined');
		// }
		// if (parentHeightmap.image !== undefined) {
		// 	// period = heightmap.image.width / Math.pow(2, factor);
		// wPeriod = parentHeightmap.image.width;
		// hPeriod = parentHeightmap.image.height;
		wPeriod = Math.pow(2, 8 - factor);
		hPeriod = Math.pow(2, 8 - factor);
	  // }
		this.logger.debug('wPeriod: ' + wPeriod);
		this.logger.debug('hPeriod: ' + hPeriod);
		// tileMesh.geometry = new THREE.PlaneGeometry(width, height);
		// tileMesh.geometry = new THREE.PlaneGeometry(width, height, wPeriod, hPeriod);
		// tileMesh.geometry.applyMatrix( new THREE.Matrix4().makeTranslation(width / 2, - height / 2, 0) );
		// // tileMesh.geometry.applyMatrix( new THREE.Matrix4().makeTranslation(width / 2, - height / 2, 0) );
		//
		// // this.logger.debug('parentHeightmap.image: ' + JSON.stringify(parentHeightmap.image));
		//
		// if (parentHeightmap.image !== undefined) {
		// 	// (red * 256 + green + blue / 256) - 32768
		// 	this.logger.debug('Asking getHeightData()');
			let data = toolbox.getHeightmapData(parentHeightmap.image, tileId, factor, 1, -32768);
		// 	this.logger.debug('data.length: ' + data.length);
		// 	this.logger.debug('data: ' + JSON.stringify(data));
			for ( var i = 0, l = tileMesh.geometry.vertices.length; i < l; i++ ) {
				// this.logger.debug('tileMesh.geometry.vertices[' + i + ']: ' + JSON.stringify(tileMesh.geometry.vertices[i]));
				tileMesh.geometry.vertices[i].z = data[i];
			}
			tileMesh.geometry.verticesNeedUpdate = true;

			// tileMesh.geometry.computeFaceNormals();
			// tileMesh.geometry.computeVertexNormals();
			// tileMesh.geometry.computeTangents();
		// }
		tileSupport.position.set(tileShift.x * width, -tileShift.y * height, 0);
	},
	getZShift: function(vertices, lon, lat, tileRef) {
		let factor = Math.min(OpenEarthView.Layer.TerrariumTerrain.factor - (tileRef.z - tileId.z), tileId.z);
		let wPeriod = Math.pow(2, 8 - factor);
		let hPeriod = Math.pow(2, 8 - factor);

		let xRate = toolbox.long2tileFloat(lon, tileRef.z) - toolbox.long2tile(lon, tileRef.z);
		let yRate = toolbox.lat2tileFloat(lat, tileRef.z) - toolbox.lat2tile(lat, tileRef.z);

		let x = Math.floor(wPeriod * xRate);
		let y = Math.floor(hPeriod * yRate);

		return vertices[x * wPeriod + y].z;
	}
}
OpenEarthView.Layer.TerrariumTerrain.factor = 5;
var R = 6378.137;
