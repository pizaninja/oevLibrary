/**
Open Earth View - library
The MIT License (MIT)
Copyright (c) 2017 Clément Igonet

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

* @author Clement Igonet
*/

var toolbox = OpenEarthView.toolbox;

OpenEarthView.Layer.OverpassBuilding = function(name, urls, options) {
    this.name = (name !== undefined) ? name : 'Overpass';
    this.minZoom = 17;
    if (options !== undefined) {
        this.minZoom = (options.minZoom !== undefined) ? options.minZoom : 18;
        this.localData = (options.localData !== undefined) ? options.localData : undefined;
        // console.log('minZoom: ' + this.minZoom);
    }
    // if (OpenEarthViewLayers.hasOwnProperty(name)) {
    //     console.err('Cannot register this already existing layer !');
    //     return;
    // }
    this.urls = (urls !== undefined) ? urls : [
        'http://overpass-api.de/api/interpreter'
    ];
};

OpenEarthView.Layer.OverpassBuilding.prototype = {
    constructor: OpenEarthView.Layer.OverpassBuilding,
    type: 'building',
    getName: function() {
        return this.name
    },
    getLocalUrl(tileId) {
        return this.localData
            .replace('${z}', tileId.z)
            .replace('${x}', tileId.x)
            .replace('${y}', tileId.y);
    },
    getUrl: function(tileId) {
        let scope = this;
        // let urls = OpenEarthViewLayers[scope.name];
        let urlRandom = this.urls[
            Math.floor(Math.random() * this.urls.length)];

        // Process GPS bounds
        minLon = toolbox.tile2long(tileId.x, tileId.z);
        maxLon = toolbox.tile2long(tileId.x + 1, tileId.z);
        minLat = toolbox.tile2lat(tileId.y + 1, tileId.z);
        maxLat = toolbox.tile2lat(tileId.y, tileId.z);

        return urlRandom
            .replace(/\${tile2long\(x\)}/g, minLon)
            .replace(/\${tile2long\(x\+1\)}/g, maxLon)
            .replace(/\${tile2lat\(y\+1\)}/g, minLat)
            .replace(/\${tile2lat\(y\)}/g, maxLat);
    }
}

var R = 6378.137;
