
OpenEarthView.Layer.Elevation = function(name, urls, options) {
    this.name = (name !== undefined) ? name : 'Normal';
}

OpenEarthView.Layer.Elevation.prototype = {
    constructor: OpenEarthView.Layer.Elevation,
    type: 'terrain',
    getName: function() {
        return this.name;
    }
    getUrl: function(zoom, xtile, ytile) {
        let urlRandom = this.urls[
            Math.floor(Math.random() * this.urls.length)];
        let url = urlRandom.replace('${z}', zoom);
        url = url.replace('${x}', xtile);
        return url.replace('${y}', ytile);
    }
}

