/*
Calls:

let requestManager = new RequestManager(loader, maxRequest);
requestManager.createRequest(z,x,y,lod);
requestManager.loadNext();
*/

let Request = require('./Request.js');
let Logger = require('../logger.js');

class RequestManager {
  constructor(localLoader, remoteLoader, maxRequest, loaderName) {
    let self = this;
    self.logger = Logger.getInstance('OpenEarthView.Request.RequestManager' + ((loaderName !== undefined) ? ('.' + loaderName) : ''));
    self.cancelRequestLogger = Logger.getInstance('OpenEarthView.Request.RequestManager.cancelOtherRequests');
    self.localLoader = localLoader;
    self.remoteLoader = remoteLoader;
    self.tileData = {};
    self.requests = {};
    self.aliveRequests = {};
    self.aliveRequestsCount = 0;
    self.requestsCount = 0;
    self.maxRequest = (maxRequest !== undefined) ? maxRequest : RequestManager.DEFAULT_MAX_REQUEST;
  }

  hasData(tileId) {
    let tilePath = tileId.z + '/' + tileId.x + '/' + tileId.y;
    return this.tileData.hasOwnProperty(tilePath);
  }

  loadCache(tileId) {
    let self = this;
    let tilePath = tileId.z + '/' + tileId.x + '/' + tileId.y;
    self.logger.debug('loadCache - tilePath: ' + tilePath, tileId);
//    let myUrl = new URL(url);
//    let tilePath = myUrl.pathname + myUrl.search;
    self.logger.debug('tileData list: ' + JSON.stringify(self.tileData), tileId);
    return (self.tileData.hasOwnProperty(tilePath)) ? (self.tileData[tilePath]) : undefined;
  }

  newRequest(tileId, localUrl, url, onLoad) {
    let self = this;
		// self.logger.debug('newRequest() - localUrl: ' + localUrl, tileId);
		// self.logger.debug('newRequest() - url: ' + url, tileId);
//    let myUrl = new URL(url);
//    let tilePath = myUrl.pathname + myUrl.search;
    let tilePath = tileId.z + '/' + tileId.x + '/' + tileId.y;
    if (self.tileData.hasOwnProperty(tilePath)) {
      // Case where data are already stored
      onLoad(self.tileData[tilePath], tileId);
    } else {
      if (self.aliveRequests.hasOwnProperty(tilePath)) {
        self.aliveRequests[tilePath].addAction(onLoad);
      } else if (!self.requests.hasOwnProperty(tilePath)) {
        // Case where data must be loaded
  //      self.logger.trace('Request for url: ' + url);
        self.requests[tilePath] =
          new Request(
            tileId,
            localUrl, self.localLoader,
            url, self.remoteLoader,
            (payload) => {
  //            let myUrl = new URL(url);
  //            let tilePath = myUrl.pathname + myUrl.search;
              let tilePath = tileId.z + '/' + tileId.x + '/' + tileId.y;
              self.logger.trace('payload loaded for tilePath: ' + tilePath, tileId);
              if (self.aliveRequests.hasOwnProperty(tilePath)) {
                delete self.aliveRequests[tilePath];
                self.aliveRequestsCount--;
                self.logger.trace('aliveRequestsCount: ' + self.aliveRequestsCount);
  //              self.tileData[tilePath] = (payload === '') ? {} : JSON.parse(payload);
                self.tileData[tilePath] = payload;
								// self.logger.debug('Calling onLoad(' +  + ') for ' + JSON.stringify(tileId));

                onLoad(payload, tileId);
/*
                for (let i = 0; i < onLoads.length; i++) {
                  onLoads[i](payload, tileId);
                }
*/
              }
              self.loadNext();
            },
            () => {},
            () => {
  //            let myUrl = new URL(url);
  //            let tilePath = myUrl.pathname + myUrl.search;
              let tilePath = tileId.z + '/' + tileId.x + '/' + tileId.y;
              if (self.aliveRequests.hasOwnProperty(tilePath)) {
                delete self.aliveRequests[tilePath];
                self.aliveRequestsCount--;
                self.logger.trace('aliveRequestsCount: ' + self.aliveRequestsCount);
              }
              self.loadNext();
            });
        self.requestsCount++;
        self.logger.trace('requestsCount:' + self.requestsCount);
        self.loadNext();
      }
    }
  }
  loadNext() {
    let self = this;
    self.logger.trace(
      'loadNext() - ' +
      'aliveRequestsCount: ' + self.aliveRequestsCount + ' - ' +
      'maxRequest: ' + self.maxRequest + ' - ' +
      'requestsCount: ' + self.requestsCount);
    while (self.aliveRequestsCount < self.maxRequest && self.requestsCount > 0) {
      let tilePaths = Object.keys(self.requests);
      self.logger.trace('tilePaths.length: ' + tilePaths.length);
      let tilePath = tilePaths[tilePaths.length - 1];
      self.logger.trace('Next tilePath to load: ' + tilePath, tilePath);
      if (self.aliveRequests.hasOwnProperty(tilePath)) {
        continue;
      }
      self.aliveRequestsCount++;
      self.logger.trace('aliveRequestsCount: ' + self.aliveRequestsCount);
      self.aliveRequests[tilePath] = self.requests[tilePath];
      let req = self.aliveRequests[tilePath];
      delete self.requests[tilePath];
      self.requestsCount--;
      self.logger.trace('requestsCount: ' + self.requestsCount);
      req.load();
    }
  }

//  cancelOtherRequests(currentIds) {
  cancelOtherRequests(currentIds) {
    let self = this;
//    let textureRequests = this.textureRequests;
//    let textureAliveRequests = this.textureAliveRequests;
    for (let id in self.requests) {
      if (!currentIds.hasOwnProperty(id)) {
        self.cancelRequestLogger.trace('delete request: ' + id);
        delete self.requests[id];
        self.requestsCount--;
      }
    }
    for (let id in self.aliveRequests) {
      if (!currentIds.hasOwnProperty(id)) {
        self.cancelRequestLogger.trace('alive request no more required: ' + id);
      }
    }
    self.loadNext();
  }
}
RequestManager.DEFAULT_MAX_REQUEST = 10;
export default RequestManager;
