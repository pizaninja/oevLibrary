```
                     _______
                    |       |
 _________          | Ready |
|         |         |_______|
|       __V_____________|________________
|      |                                 |
|      - newRequest()                    - newRequest()
|      | & jsonData[tilePath] != null    | & jsonData[tilePath] == null
|      |                                 | & requests[tilePath] == null
|      |                            _____|____
|      |                           |          |
|      |                           | Request  |
|      |                           |__________|
|      |      ___________________________|
|      |     |                           |                                  |
|      |     - finish()                  - failure()                        |
|      |     |                           |__________________________________|
|      |     |
|      |     |
|      |     |
|   ___V_____V___
|  |             |
|  | ProcessData |
|  |_____________|
|        |
|________|
```
