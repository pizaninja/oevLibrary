import State from './State';

let Logger = require('../../logger.js');
class Ready4LocalLoadState extends State {
    constructor(request) {
        super();
        this.logger_ = Logger.getInstance('OpenEarthView.Request.Ready4LocalLoadState');
        this._request = request;
        // this.logger_.debug('constructor() - request: ' + JSON.stringify(this._request));
    }

    load() {
        let self = this;
        this.logger_.trace('load()');
        let url;
        let loader;
//        this.logger_.trace('load() - request: ' + JSON.stringify(this._request));
        let localUrl = this._request.localUrl;
				// self.logger_.debug('this._request.localUrl: ' + this._request.localUrl);
				// self.logger_.debug('this._request.remoteUrl: ' + this._request.remoteUrl);
        if (localUrl !== undefined && localUrl !== null) {
            url = this._request.localUrl;
            loader = this._request.localLoader;
            this._request.setState(this._request.getLocalLoadingState());
        } else {
            url = this._request.remoteUrl;
            loader = this._request.remoteLoader;
            this._request.setState(this._request.getRemoteLoadingState());
        }
        self.logger_.debug('Start loading: ' + url);
        // load: function ( url, onLoad(response), onProgress(event), onError(event) ) {}
        ((url) => {
        loader.load(
            url,
            (response) => {
                self.logger_.debug('Success in loading: ' + url);
                self._request.state.success(response);
            },
            (event) => {
                self.logger_.trace('Load in progress: ' + url);
                this._request.state.progress(event);
            },
            (event) => {
                self.logger_.error('Error loading: ' + url);
                self.logger_.trace('Event: ' + JSON.stringify(event));
                this._request.state.fail(event);
            });
        })(url);
    }

    progress(event) {
        throw new Error("You can't make progress a not started load!");
    }

    fail(event) {
        throw new Error("A load can't fail if is not started!");
    }

    success(response) {
        throw new Error("A load can't success if is not started!");
    }
}

export default Ready4LocalLoadState;
