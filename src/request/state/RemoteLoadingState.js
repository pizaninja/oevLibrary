import State from './State';

class RemoteLoadingState extends State {
    constructor(request) {
        super();
        this._request = request;
    }

    load() {
        throw new Error("You can't load a file that is being loaded already!");
    }

    progress(event) {
        this._request.onProgress();
    }

    fail(event) {
        this.logger.error('remote tile load failed: ' + JSON.stringify(this._request.tileCoord));
        this._request.onFailure();
        this._request.setState(this._request.getLoadedFailedState());
    }

    success(response) {
        this.logger.trace('remote tile loaded: ' + JSON.stringify(this._request.tileCoord));
        // console.log('response:', response);
        this._request.onFinish(response);
        this._request.setState(this._request.getLoadedState());
    }
}

export default RemoteLoadingState;
