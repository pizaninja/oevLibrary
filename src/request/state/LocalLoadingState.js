import State from './State';

let Logger = require('../../logger.js');
class LocalLoadingState extends State {
    constructor(request) {
        super();
        this.logger = Logger.getInstance('OpenEarthView.Request.State');
        this._request = request;
    }

    load() {
        throw new Error("You can't load a file that is being loaded already!");
    }

    progress(event) {
        // console.log('Getting local data in progress: ', this._request.localUrl);
        this._request.onProgress();
    }

    fail(event) {
        // console.log('Fail to get local data at', this._request.localUrl);
        this._request.setState(this._request.getReady4RemoteLoadState());
        this._request.load();
    }

    success(response) {
        this.logger.trace('Success in getting local data at ' + this._request.localUrl);
        this._request.onFinish(response);
        this._request.setState(this._request.getLoadedState());
    }
}

export default LocalLoadingState;
