/**
Open Earth View - viewer-threejs
The MIT License (MIT)
Copyright (c) 2016 Clément Igonet

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
/the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

* @author Clement Igonet
*/

// export default OpenEarthView.World = {};
//   //
// }
// require("imports-loader?toolbox=OpenEarthView.toolbox!./openearthview.js");

// OpenEarthView.Terrain.FlatTerrain = require("./terrains/FlatTerrain.js");
// var FlatTerrain = require('./terrains/flatTerrain.js');
let toolbox = require('./toolbox.js');
let tileLoader = require('./loader/TileLoader.js');
let FlatTerrain = require('./layer/FlatTerrain.js');
//let R = 6378.137;
let THREE = require('THREE');
THREE.OEVShaderTerrain = require('./shader/OEVShaderTerrain.js');
// let oevShaderTerrain = THREE.OEVShaderTerrain['terrain'];

let Logger = require('./logger.js');
let Stats = require('../node_modules/three/examples/js/libs/stats.min.js');
// let Stats = require()

// THREE.EarthControls = require('./controls/EarthControls_class.js');
// THREE.EarthControls = require('./controls/EarthControls_function.js');
let self;
class World {
  constructor(domElement) {
    self = this;
    self.logger = Logger.getInstance('OpenEarthView.World');
    self.logger.info('World logger at info level');
    // self.goUpdateSceneLazy = () => {
    //     self.updateSceneLazy();
    // };
    this.selectedBuilding = null;
    this.domElement = domElement;
    this.layers = {};
    this.loaders = {};
		this.layers['defaultTerrainLayer'] = new FlatTerrain('flatTerrain', null, null);
    this.loaders['defaultTerrainLayer'] = undefined;

    if (this.domElement === null) {
      alert('No domElement defined.');
      return;
    }
	this.lastSelectedBuilding = {
		id: undefined,
		tileMesh: undefined,
		bboxMesh: undefined,
		bboxCenter: undefined,
		building: undefined
	};

    // if (this.layers.length === 0) {
    //   this.layers[0] = new OpenEarthView.Layer.OSM("defaultLayer");
    // }
    this.ZOOM_SHIFT_SIZE = 4;
//    this.ZOOM_SHIFT_SIZE = 1;
    this.MAX_TILEMESH = 400;
    // this.ZOOM_FLAT = 13;
    this.ZOOM_MIN = 1;
    this.tileMeshes = {};
    this.tileMeshQueue = [];
    this.LONGITUDE_ORI = -73.98468017578125;
    this.LATITUDE_ORI = 40.7477771608207;
    this.R = 6378.137;
    this.xtile = 0;
    this.ytile = 0;
    this.zoom = 0;
    // Set of tileGroup[] for a given scene (defined by the camera position)
    this.tileGroups;
    // Table of tiles of a given zoom
    this.tileGroup = [];
    this.defaultAlti = 150;
    // this.geojsonLoader = new THREE.GeojsonLoader();
    // this.geojsonLoader = THREE.GeojsonLoader.getSingleton();

    this.camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 100000000);
    // console.log('camera:', JSON.stringify(this.camera));
    this.camera.up.set(0, 0, 1);

    this.domElement = document.getElementById(domElement);
    // console.log('container.clientWidth:', container.clientWidth)
    // console.log('container.clientHeight:', container.clientHeight)
    // document.body.appendChild(this.canvas);

    // var background = document.createElement('Background');
    // background.setAttribute('groundColor', '0.972 0.835 0.666');
    // background.setAttribute('skyAngle', '1.309 1.571');
    // background.setAttribute('skyColor', '0.0 0.2 0.7 0.0 0.5 1.0 1.0 1.0 1.0');
    // scene.appendChild(background);
    this.canvas = document.createElement('canvas');
    this.canvas.setAttribute('id', 'openearthviewcanvas');
    this.domElement.appendChild(this.canvas);

		this.stats = new Stats();
		// this.stats.showPanel(0);
		// this.stats.showPanel(2);
    this.domElement.appendChild(this.stats.dom);

    this.renderer = new THREE.WebGLRenderer({
      canvas: this.canvas
    });
    this.canvas.width = this.canvas.clientWidth;
    this.canvas.height = this.canvas.clientHeight;
    // renderer.setViewport(0, 0, canvas.clientWidth, canvas.clientHeight);
    //
    this.renderer.setSize(this.canvas.clientWidth, this.canvas.clientHeight);
    // this.canvas.appendChild(this.renderer.domElement);

    this.renderer.shadowMapEnabled = true;
    this.renderer.shadowMapSoft = false;

    this.renderer.shadowCameraNear = 3;
    this.renderer.shadowCameraFar = this.camera.far;
    this.renderer.shadowCameraFov = 50;

    this.renderer.shadowMapBias = 0.0039;
    this.renderer.shadowMapDarkness = 0.5;
    this.renderer.shadowMapWidth = 1024;
    this.renderer.shadowMapHeight = 1024;

    this.lonStamp = 0;
    this.latStamp = 0;
    this.altitude = this.defaultAlti;
    this.scene = new THREE.Scene();

    // document.body.appendChild(renderer.domElement);
    this.buildingObjects = [];
    this.earth = new THREE.Object3D();
    this.earth.position.set(0, 0, -this.R * 1000);
    this.scene.add(this.earth);

    // this.selectedBuildingContainer = new THREE.Object3D();
    // this.scene.add(this.selectedBuildingContainer);

    let light1 = new THREE.DirectionalLight(0xf0f0e0, 1);
    let light2 = new THREE.DirectionalLight(0xf0f0e0, 1);
    let light3 = new THREE.DirectionalLight(0xffffe0, 1);

    light1.position.set(10000, 0, 100000);
    light2.position.set(-5000, 8700, 10000);
    light3.position.set(-5000, -8700, 1000);
    // var light4 = new THREE.DirectionalLight(0xffffff, 1);
    // light4.position.set(-10000, 10000, 20000);
    this.scene.add(light1);
    this.scene.add(light2);
    this.scene.add(light3);
    // this.scene.add(light4);

    document.addEventListener('keydown', this.onDocumentKeyDown, false);
    this.camera.position.z = this.altitude;
    this.controls = new THREE.EarthControls(
      this.camera,
      this.renderer.domElement,
      () => {
        self.render();
      },
      () => {
        self.updateSceneLazy();
      }, {
        longitude: this.LONGITUDE_ORI,
        latitude: this.LATITUDE_ORI
      },
			(raycaster) => {
        this.onSelectObject(raycaster);
      },
			(raycaster) => {
        this.onOpenObject(raycaster);
      }
  );

    // var canvas = document.getElementById('world');
    // this.canvas.addEventListener('resize', this.onWindowResize, false);
    window.addEventListener(
      'resize',
      () => {
      // console.log('coucou !!!');
      // console.log('this.domElement.clientWidth:', this.domElement.clientWidth);
        var width = self.domElement.clientWidth;
        var height = self.domElement.clientHeight;

        self.renderer.setViewport(0, 0, width, self.canvas.height);
        self.renderer.setSize(width, height);
        self.camera.aspect = width / height;
        self.camera.updateProjectionMatrix();
        self.render();
      },
      false);

    //   canvas.addEventListener('resize', function () {
    //   canvas.width  = canvas.clientWidth;
    //   canvas.height = canvas.clientHeight;
    //   renderer.setViewport(0, 0, canvas.clientWidth, canvas.clientHeight);
    //   camera.aspect = canvas.clientWidth / canvas.clientHeight;
    //   camera.updateProjectionMatrix();
    // });

    // canvas.width  = canvas.clientWidth;
    // canvas.height = canvas.clientHeight;
    // renderer.setViewport(0, 0, canvas.clientWidth, canvas.clientHeight);
    // camera.aspect = canvas.clientWidth / canvas.clientHeight;
    // camera.updateProjectionMatrix();

    // this.onWindowResize = function() {
    //   console.log("Call to onWindowResize.");
    //   // var container = document.getElementById("world");
    //   this.renderer.setSize(this.canvas.clientWidth, this.canvas.clientHeight);
    //   this.camera.aspect = this.canvas.clientWidth / this.canvas.clientHeight;
    //   this.camera.updateProjectionMatrix();
    //
    //   // renderer.setSize(window.innerWidth, window.innerHeight);
    //   // render();
    // }
  }
  // var scope = this;
	onOpenObject(raycaster) {
		for (let layerId in self.layers) {
			switch (self.layers[layerId].type) {
        case 'building':
				let building = self.getBuildingFromRaycaster(raycaster);
				// Check if building already selected previously
				if (self.lastSelectedBuilding.id !== undefined &&
					building !== undefined &&
					building.userData.osm.id === self.lastSelectedBuilding.id) {
					self.logger.debug('Open the building !');
					// TODO: switch to building navigation
					self.controls.switchNavigationMode(
						THREE.EarthControls.NAV_MODE_BUILDING,
						self.lastSelectedBuilding
					);
				}
				break;
			}
		}
	}

	getBuildingFromRaycaster(raycaster) {
		let building;
		let intersects = raycaster.intersectObjects(self.buildingObjects, false);
		if (intersects.length > 0) {
			let distance = 0;
			let intersect = null;
			for (let idx = 0; idx < intersects.length; idx++) {
				if (distance === 0 || intersects[idx].distance < distance) {
					intersect = intersects[idx];
					distance = intersect.distance;
				}
			}
			if (intersect !== null && intersect.object.hasOwnProperty('geometry')) {
				building = intersect.object.parent;
			};
		}
		return building;
	}

	onSelectObject(raycaster) {
    let logger_ = Logger.getInstance('OpenEarthView.World.onSelectObject');
    for (let layerId in self.layers) {
      logger_.trace(
        'self.layers[layerId].type:' + JSON.stringify(self.layers[layerId].type));
      switch (self.layers[layerId].type) {
        case 'building':
          logger_.debug('self.buildingObjects: ' + self.buildingObjects);
					let building = self.getBuildingFromRaycaster(raycaster);

          if (building !== undefined) {
						logger_.debug('Selected building: ' + building.userData.osm.id);

						// Stop if building already selected
						if (self.lastSelectedBuilding.id !== undefined && building.userData.osm.id === self.lastSelectedBuilding.id) {
							logger_.debug('Building already selected !');
							break;
						}

						// Remove previous selection.
						if (self.lastSelectedBuilding.id !== undefined && building.userData.osm.id !== self.lastSelectedBuilding.id) {
							logger_.debug('Previous building: ' + self.lastSelectedBuilding.id);
							self.lastSelectedBuilding.tileMesh.remove(self.lastSelectedBuilding.bboxMesh);
							self.lastSelectedBuilding.tileMesh.remove(self.lastSelectedBuilding.bboxCenter);
							for (let i = self.lastSelectedBuilding.building.children.length - 1; i >= 0; i--) {
								let buildingBlockMesh = self.lastSelectedBuilding.building.children[i];
								buildingBlockMesh.material.transparent = false;
							}
							// scope.lastSelectedBuildinguildingBlockMesh.material.transparent = true;
						}

						// Transparency for all building elements
						for (let i = building.children.length - 1; i >= 0; i--) {
							let buildingBlockMesh = building.children[i];
							buildingBlockMesh.material.transparent = true;

							// buildingBlockMesh.material = new THREE.MeshPhongMaterial({
							//     color: buildingBlockMesh.material.color,
							//     transparent: true,
							//     opacity: 0.4
							// });
						}

						let tileMesh = building.parent;

            // logger_.trace('intersect: ' + intersect);
            // logger_.trace('intersect.object: ' + JSON.stringify(intersect.object));
            // logger_.trace('intersect.object.userData: ' + intersect.object.userData);
            // logger_.trace('building: ' + building);

            // Tutorial - Process bounding box - method setFromObject
            // Scene coordinates.
            // let setFromObject = new THREE.Box3().setFromObject(building);

            // Tutorial - Process bounding box - method computeBoundingBox
            // Bad: not hierarchical.
            // Target object coordinates.
            // buildingBlock.object.geometry.computeBoundingBox();
            // let computeBoundingBox = buildingBlock.object.geometry.boundingBox.clone();

            // Tutorial - Process bounding box - method BoxHelper
            // let bboxMesh = new THREE.BoxHelper(building);
						let bboxMesh = toolbox.boundingBoxMesh(building);

						logger_.trace('bboxMesh: ' + JSON.stringify(bboxMesh));
						let bboxWorldPos = new THREE.Vector3();
						bboxWorldPos.setFromMatrixPosition(tileMesh.matrixWorld);
						logger_.trace('bboxWorldPos: ' + JSON.stringify(bboxWorldPos));
						logger_.trace('bboxWorldPos.x: ' + JSON.stringify(bboxWorldPos.x));
						bboxMesh.position.setX(-bboxWorldPos.x);
						bboxMesh.position.setY(-bboxWorldPos.y);
						// bboxMesh.position.set(-bboxWorldPos.x, -bboxWorldPos.y, 0);

						bboxMesh.add(toolbox.originAxes(4, 1000));
						logger_.trace('bboxMesh.position: ' + JSON.stringify(bboxMesh.position));

            let bboxCenter = new THREE.Object3D();
            bboxCenter.position.set(
              bboxMesh.geometry.boundingSphere.center.x - bboxWorldPos.x,
              bboxMesh.geometry.boundingSphere.center.y - bboxWorldPos.y,
              bboxMesh.geometry.boundingSphere.center.z - bboxWorldPos.z);
							logger_.trace('bboxCenter.position: ' + JSON.stringify(bboxCenter.position));
            bboxCenter.add(toolbox.originAxes(4, 1000));

            tileMesh.add(bboxCenter);
            tileMesh.add(bboxMesh);

            // Add axis to scene
            // self.scene.add(toolbox.originAxes(4, 1000));
            logger_.trace(
              'building.userData.info.tags: ' + building.userData.osm.tags.building);

            let message = 'id: ' + building.userData.osm.id;
            if (building.userData.osm.tags.hasOwnProperty('name')) {
							message = building.userData.osm.tags.name;
						} else if (building.userData.osm.tags.hasOwnProperty('addr:street')) {
							message = (building.userData.osm.tags.hasOwnProperty('addr:housenumber')) ? building.userData.osm.tags['addr:housenumber'] : '';
							message = building.userData.osm.tags['addr:street'];
						}

            let spritey = toolbox.makeTextSprite(message, {
              fontsize: 24,
              borderColor: {
                r: 255,
                g: 0,
                b: 0,
                a: 1.0
              },
              backgroundColor: {
                r: 255,
                g: 100,
                b: 100,
                a: 0.8
              }
            });
            // let spritey2 = toolbox.makeTextSprite('abcdefghijkl', {
            //   fontsize: 24,
            //   borderColor: {
            //     r: 255,
            //     g: 0,
            //     b: 0,
            //     a: 1.0
            //   },
            //   backgroundColor: {
            //     r: 255,
            //     g: 100,
            //     b: 100,
            //     a: 0.8
            //   }
            // });
            // let spritey3 = toolbox.makeTextSprite(
            //   'abcdefghijklmnopqrstuv', {
            //     fontsize: 24,
            //     borderColor: {
            //       r: 255,
            //       g: 0,
            //       b: 0,
            //       a: 1.0
            //     },
            //     backgroundColor: {
            //       r: 255,
            //       g: 100,
            //       b: 100,
            //       a: 0.8
            //     }
            //   });

            spritey.position.setZ(bboxMesh.geometry.boundingSphere.radius);
            // spritey2.position.setZ(bboxMesh.geometry.boundingSphere.radius + 35);
            // spritey3.position.setZ(bboxMesh.geometry.boundingSphere.radius + 70);

            logger_.trace('spritey: ' + spritey);

            bboxCenter.add(spritey);
            // bboxCenter.add(spritey2);
            // bboxCenter.add(spritey3);

						// Set last selected building as current building
						self.lastSelectedBuilding.id = building.userData.osm.id;
						self.lastSelectedBuilding.tileMesh = tileMesh;
						self.lastSelectedBuilding.bboxMesh = bboxMesh;
						self.lastSelectedBuilding.bboxCenter = bboxCenter;
						self.lastSelectedBuilding.building = building;

            self.render();
          }

          /*
          // Parse all the faces
          for ( var i in intersects ) {

          	intersects[ i ].face.material[ 0 ].color.setHex( Math.random() * 0xffffff | 0x80000000 );

          }
          */
          break;
      }
    }

    // console.log('intersects:', JSON.stringify(intersects));
    //
    // if (intersects.length > 0) {
    //   intersects[0].object.material.color.setHex(Math.random() * 0xffffff);
    // }
  }

  // Add a layer (tile, building or terrain)
  addLayer(openEarthViewLayer, openEarthViewLoader) {
    let layerName = openEarthViewLayer.getName();
    self.logger.info('Add layer: ' + layerName);
		if (openEarthViewLayer.type === 'tile') {
			if (this.layers.hasOwnProperty('defaultTileLayer')) {
				delete this.layers['defaultTileLayer'];
			}
			if (this.loaders.hasOwnProperty('defaultTileLoader')) {
				delete this.loaders['defaultTileLoader'];
			}
		}
		if (openEarthViewLayer.type === 'terrain') {
			if (this.layers.hasOwnProperty('defaultTerrainLayer')) {
				delete this.layers['defaultTerrainLayer'];
			}
			if (this.loaders.hasOwnProperty('defaultTerrainLoader')) {
				delete this.loaders['defaultTerrainLoader'];
			}
		}
		if (openEarthViewLayer.type === 'building') {
			if (this.layers.hasOwnProperty('defaultBuildingLayer')) {
				delete this.layers['defaultBuildingLayer'];
			}
			if (this.loaders.hasOwnProperty('defaultBuildingLoader')) {
				delete this.loaders['defaultBuildingLoader'];
			}
		}

		// if (this.layers.hasOwnProperty('defaultLayer')) {
    //   delete this.layers['defaultLayer'];
    // }
    // if (this.layers.length === 1 && this.layers[0].getName() === "defaultLayer") {
    //   this.layers.pop();
    // }
    // this.layers[this.layers.length] = openEarthViewLayer;
    this.layers[layerName] = openEarthViewLayer;
    this.loaders[layerName] = openEarthViewLoader;
/*
    // Load big earth tiles
    if (openEarthViewLayer.type === 'tile') {
      for (let xtile = 0; xtile < 4; xtile++) {
        for (let ytile = 0; ytile < 4; ytile++) {
          tileLoader.load({
              z: 2,
              x: xtile,
              y: ytile
            },
            openEarthViewLayer.getLocalUrl(2, xtile, ytile),
            openEarthViewLayer.getUrl(2, xtile, ytile),
            function onLoad(texture) {
              // tileMesh.material.map = texture;
              // tileMesh.material.needsUpdate = true;
              // this.render();
            }
          );
        }
      }
    }
*/
  }

  // this.addTerrain("defaultTerrain", new OpenEarthView.Terrain.FlatTerrain("FlatTerrain"));
  // world.addTerrain(new OpenEarthView.Terrain.FlatTerrain("FlatTerrain"));

/*
  addTerrain(openEarthViewTerrain) {
    console.log('Add terrain:', openEarthViewTerrain.getName());
    if (this.terrains.hasOwnProperty('defaultTerrain')) {
      delete this.terrains['defaultTerrain'];
    }
    this.terrains[openEarthViewTerrain.getName()] = openEarthViewTerrain;
  };
*/
  // for (layerIdx = 0; layerIdx < layers.length; layerIdx++) {
  //   this.addLayer(layers[layerIdx]);
  // }
  //

  // var toolbox = OpenEarthView.toolbox;
  // var terrainLoader = new OpenEarthView.TerrainLoader();

  // this.updateSceneLazy();

	render() {
			// requestAnimationFrame(render);
			// //////////////////////////////////////////////////////////
			// var oldXtile;
			// var oldYtile;
			// var oldZoom = this.zoom;
			// var dist = new THREE.Vector3().copy(this.controls.object.position).sub(this.controls.target).length();
			// var zoom__ = Math.floor(Math.max(Math.min(Math.floor(27 - Math.log2(dist)), 19), 1));

			// if (zoom__ > this.ZOOM_MIN) {
			//     this.zoom = zoom__;
			// }

			if (this.lonStamp !== this.controls.getLongitude() ||
					this.latStamp !== this.controls.getLatitude()) {
					// this.lonStamp = this.controls.getLongitude();
					// this.latStamp = this.controls.getLatitude();
					this.earth.rotation.set(
							this.controls.getLatitude() * Math.PI / 180,
							(-this.controls.getLongitude()) * Math.PI / 180,
							0);
					// oldXtile = this.xtile;
					// oldYtile = this.ytile;
					// console.log('toolbox:', toolbox);
					// this.xtile = toolbox.long2tile(this.lonStamp, this.zoom);
					// this.ytile = toolbox.lat2tile(this.latStamp, this.zoom);

					// if (Math.abs(oldXtile - this.xtile) >= 1 ||
					//     Math.abs(oldYtile - this.ytile) >= 1) {
					//     this.updateScene({
					//         'lon': this.lonStamp,
					//         'lat': this.latStamp,
					//         'alti': this.altitude
					//     });
					// }
			}
			// else if (Math.abs(this.zoom - oldZoom) >= 1) {
			//     this.updateScene({
			//         'lon': this.lonStamp,
			//         'lat': this.latStamp,
			//         'alti': this.altitude
			//     });
			// }

			// //////////////////////////////////////////////////////////
			this.stats.update();
			this.renderer.render(this.scene, this.camera);
	}

  // Update scene only if x, y or z or changing enough
  updateSceneLazy() {
      // requestAnimationFrame(render);
      // //////////////////////////////////////////////////////////
      var oldXtile;
      var oldYtile;
      var oldZoom = this.zoom;
      var dist = new THREE.Vector3().copy(this.controls.object.position).sub(this.controls.target).length();
      var zoom__ = Math.floor(Math.max(Math.min(Math.floor(27 - Math.log2(dist)), 19), 1));

      if (zoom__ > this.ZOOM_MIN) {
        this.zoom = zoom__;
      }

      if (this.lonStamp !== this.controls.getLongitude() ||
        this.latStamp !== this.controls.getLatitude()) {
        this.lonStamp = this.controls.getLongitude();
        this.latStamp = this.controls.getLatitude();
        this.earth.rotation.set(
          this.controls.getLatitude() * Math.PI / 180,
          (-this.controls.getLongitude()) * Math.PI / 180,
          0);
        oldXtile = this.xtile;
        oldYtile = this.ytile;
        // console.log('toolbox:', toolbox);
        this.xtile = toolbox.long2tile(this.lonStamp, this.zoom);
        this.ytile = toolbox.lat2tile(this.latStamp, this.zoom);

        if (Math.abs(oldXtile - this.xtile) >= 1 ||
          Math.abs(oldYtile - this.ytile) >= 1) {
          this.updateScene({
            'lon': this.lonStamp,
            'lat': this.latStamp,
            'alti': this.altitude
          });
        }
      } else if (Math.abs(this.zoom - oldZoom) >= 1) {
        this.updateScene({
          'lon': this.lonStamp,
          'lat': this.latStamp,
          'alti': this.altitude
        });
      }

      // //////////////////////////////////////////////////////////
      this.renderer.render(this.scene, this.camera);
    }
/*
scene
-> earth
  -> tileGroups
    -> tileGroup[zoom_]
    (zoom_ < World.ZOOM_FLAT) =>
      -> tileEarth
        -> tileMesh
    (zoom >= World.ZOOM_FLAT) =>
    -> oriLonRot
      -> oriLatRot
        -> oriGround
        for each tile:
          -> tileSupport
            -> tileMesh (ground texture)
            -> obj (building)
*/
 // Update scen content
	clearEarth() {
		// let tileGroups_ = this.tileGroups;
		let oriLonRot = this.tileGroups.getObjectByName('oriLonRot');
		if (oriLonRot !== undefined) {
			let oriLatRot = oriLonRot.getObjectByName('oriLatRot');
			let oriGround = oriLatRot.getObjectByName('oriGround');
			// remove all tileSupports
			for (let i = 0; i < oriGround.children.length; i++) {
				let tileSupport = oriGround.children[i];

				// Remove little tile mesh
				let tileMesh = tileSupport.getObjectByName('tileMesh');
				tileSupport.remove(tileMesh);
				tileMesh.geometry.dispose();
				tileMesh.material.map.dispose();
				tileMesh.material.dispose();
				tileMesh.dispose();
				tileMesh = undefined;

				let buildings = tileSupport.getObjectByName('buildings');
				if (buildings !== undefined) {
					tileSupport.remove(buildings);
					buildings.removeAll();
				}

				oriGround.remove(tileSupport);
			}
			oriLatRot.remove(oriGround);
			oriLonRot.remove(oriLatRot);
			this.tileGroups.remove(oriLonRot);
		}
		this.earth.remove(this.tileGroups);
		// Remove all big tile mesh
		let tileMesh = this.earth.getObjectByName('tileMesh');
		while(tileMesh !== undefined) {
			this.earth.remove(tileMesh);
			tileMesh = this.earth.getObjectByName('tileMesh');
		}
	}
  updateScene(position) {
		this.setZShift(0);

    let tiles = {};
    let currentIds = {};
    let zoomMax;
    let zoomMin;
    let oriGround;
    this.xtile = toolbox.long2tile(position.lon, this.zoom);
    this.ytile = toolbox.lat2tile(position.lat, this.zoom);
		this.clearEarth();
    this.buildingObjects = [];
    this.tileGroups = new THREE.Object3D();
    this.earth.add(this.tileGroups);
    oriGround = new THREE.Object3D();
		oriGround.name = 'oriGround';
    if (this.zoom >= World.ZOOM_FLAT) {
      let xtileOri = toolbox.long2tile(position.lon, World.ZOOM_FLAT);
      let ytileOri = toolbox.lat2tile(position.lat, World.ZOOM_FLAT);
      let lonOri = toolbox.tile2long(xtileOri, World.ZOOM_FLAT);
      let latOri = toolbox.tile2lat(ytileOri, World.ZOOM_FLAT);
      let oriLatRot = new THREE.Object3D();
			oriLatRot.name = 'oriLatRot';
      let oriLonRot = new THREE.Object3D();
			oriLonRot.name = 'oriLonRot';

      // 3 - ground position
      oriGround.position.set(0, 0, this.R * 1000);
      // 2 - Latitude rotation
      oriLatRot.rotation.set((-latOri) * Math.PI / 180, 0, 0);
      // 1 - Longitude rotation
      oriLonRot.rotation.set(0, lonOri * Math.PI / 180, 0);

      this.tileGroups.add(oriLonRot);
      oriLonRot.add(oriLatRot);
      oriLatRot.add(oriGround);
    }

    self.logger.info('position: ' + JSON.stringify({
      zoom: this.zoom,
      xtile: this.xtile,
      ytile: this.ytile
    }));
    zoomMax = Math.max(this.zoom, this.ZOOM_MIN);
    zoomMin = Math.max(this.zoom - this.ZOOM_SHIFT_SIZE, this.ZOOM_MIN);

    for (let zoom_ = zoomMax; zoom_ > zoomMin; zoom_--) {
      let zShift = this.zoom - zoom_;
      let factor;
      let tileRef;
      let size;
      let minXtile;
      let maxXtile;
      let minYtile;
      let maxYtile;
      let modulus;

      this.tileGroup[zShift] = new THREE.Object3D();
      this.tileGroups.add(this.tileGroup[zShift]);

      if (zoom_ < 0 && zShift > 0) {
        continue;
      }
      factor = Math.pow(2, zShift);
      tileRef = {
				x: Math.floor(this.xtile / factor),
        y: Math.floor(this.ytile / factor),
				z: this.zoom
			};
      size = 2;
/*
      if (this.zoom < 8 && zoom_ < 6) {
        size = 2;
      } else if (zoom_ < 19) {
        size = 2;
      }
*/
      minXtile = Math.max(0, Math.floor((tileRef.x - (Math.pow(2, (size - 1)) - 1)) / 2) * 2);
      maxXtile = Math.floor((tileRef.x + (Math.pow(2, (size - 1)) - 1)) / 2) * 2 + 1;
      minYtile = Math.max(0, Math.floor((tileRef.y - (Math.pow(2, (size - 1)) - 1)) / 2) * 2);
      maxYtile = Math.floor((tileRef.y + (Math.pow(2, (size - 1)) - 1)) / 2) * 2 + 1;
      modulus = (zoom_ > 0) ? Math.pow(2, zoom_) : 0;

      // minXtile = xtile_;
      // maxXtile = xtile_;
      // minYtile = ytile_;
      // maxYtile = ytile_;
      for (let atile = minXtile; atile <= maxXtile; atile++) {
        for (let btile = minYtile; btile <= maxYtile; btile++) {

					// Exclude tiles at lower zoom already covered by current zoom_
          let id = 'z_' + zoom_ + '_' + (atile % modulus) +
            '_' + (btile % modulus);
          for (let zzz = 1; zzz <= 2; zzz++) {
            let idNext;

            idNext = 'z_' + (zoom_ - zzz) +
              '_' + Math.floor((atile % modulus) / Math.pow(2, zzz)) +
              '_' + Math.floor((btile % modulus) / Math.pow(2, zzz));
            tiles[idNext] = {};
          }
					if (!tiles.hasOwnProperty(id)) {
            // tileSupport will contain tll layers (terrain elevation, ground texture, building objs, etc...)
            let tileSupport;
            // tileMesh geometry is terrain with elevation, tileMesh texture is OSM pictures
            let tileMesh = new THREE.Mesh();
						tileMesh.name = 'tileMesh';
						// let terrainShader = THREE.ShaderTerrain;
						// let uniformsTerrain = THREE.UniformsUtils.clone(terrainShader.uniforms);

						// let uniformsTerrain = THREE.UniformsUtils.clone(THREE.OEVShaderTerrain.uniforms);
						// self.logger.debug('uniformsTerrain: ' + JSON.stringify(uniformsTerrain));
						// let uniformsTerrain = THREE.OEVShaderTerrain
						// let uniformsTerrain = {
						// 	map: {value: null},
						// 	displacementMap: {value: null},
						// 	displacementBias: {value: 0.0},
						// 	displacementScale: {value: 1.0}
						// };
						// uniformsTerrain['map'].value = null;
						// uniformsTerrain['displacementMap'].value = null;
						// uniformsTerrain['displacementBias'].value = 0.0;
						// uniformsTerrain['displacementScale'].value = 1.0;
						// self.logger.debug('THREE.OEVShaderTerrain.fragmentShader: ' + THREE.OEVShaderTerrain.fragmentShader);
						// tileMesh.material = new THREE.ShaderMaterial({
						// 	uniforms: uniformsTerrain,
						// 	fragmentShader: THREE.OEVShaderTerrain.fragmentShader,
						// 	vertexShader: THREE.OEVShaderTerrain.vertexShader
						// });
						// tileMesh.material = new THREE.MeshPhongMaterial();

            // tileMesh.position.set(0, 0, -10);
            for (let layerId in this.layers) {
              if (!this.layers.hasOwnProperty(layerId)) {
                continue;
              }
              let layer = self.layers[layerId];
              let loader = self.loaders[layerId];
							self.logger.trace('Layer: ' + JSON.stringify(layer));
							// self.logger.debug('Loader: ' + JSON.stringify(loader));
              switch (layer.type) {
                case 'terrain':
                  if (zoom_ < World.ZOOM_FLAT) {
                    let tileEarth = new THREE.Object3D(); // create an empty container
                    tileEarth.rotation.set(0, (toolbox.tile2long(atile, zoom_) + 180) * Math.PI / 180, 0);
                    self.tileGroup[zShift].add(tileEarth);
                    tileMesh.geometry = toolbox.singleton.getTileMesh(self.R, zoom_, btile, Math.max(9 - zoom_, 0)).clone();
                    tileEarth.add(tileMesh);
                  } else {
										let tileShift = {
											x: (atile - tileRef.x) + (tileRef.x % Math.pow(2, zoom_ - World.ZOOM_FLAT)),
											y: (btile - tileRef.y) + (tileRef.y % Math.pow(2, zoom_ - World.ZOOM_FLAT))
										};
										tileSupport = new THREE.Object3D(); // create an empty container
										oriGround.add(tileSupport);
										tileSupport.add(tileMesh);
										if (self.logger.getLogLevel() >= Logger.TRACE) {
											oriGround.add(toolbox.originAxes(40, 10000));
										}

										// self.logger.debug('layer: ' + JSON.stringify(layer));
                    ((tileSupport, tileMesh, tileId, tileShift, lod_) => {
											layer.preProcess(tileSupport, tileMesh, tileId, tileShift, tileRef);
											if (loader !== undefined && loader !== null) {
												// self.logger.debug('layer: ' + JSON.stringify(layer));
												const sourceTileId = layer.getSourceTileId(tileId, this.zoom);
                        const localUrl = layer.getLocalUrl(sourceTileId);
                        const url = layer.getUrl(sourceTileId);
												// layer.preProcess(tileSupport, tileMesh, tileId, tileShift, tileRef);

                        loader.load(
													sourceTileId,
                          localUrl,
                          url,
                          heightmap => {
														// let logger_ = Logger.getInstance('OpenEarthView.World.updateScene()->terrain');
														// logger_.trace('sourceTileId: ' + JSON.stringify(sourceTileId), sourceTileId);
														// // if (JSON.stringify(sourceTileId) === JSON.stringify({z: 13, x: 4252, y: 2919})) {
														// if (JSON.stringify(tileId) === JSON.stringify({z: 16, x: 34016, y: 23358})) {
														// 	// logger_.debug('tileId: ' + JSON.stringify(tileId));
														// 	// {"z":16,"x":34019,"y":23359}
														// 	tileMesh.material.uniforms['displacementScale'].value = +0.005;
														// 	tileMesh.material.uniforms['displacementBias'].value = -32768.0;
														// 	tileMesh.material.uniforms['displacementMap'].value = heightmap;
														// 	tileMesh.material.needsUpdate = true;
														// 	self.render();
														// }

														// tileMesh.material.uniforms['displacementMap'].value = heightmap;
														// console.log('heightmap received for ' + JSON.stringify(tileId));

														// layer.process(heightmap, tileSupport, tileMesh, tileId, tileShift, tileRef);
														// tileMesh.material.uniforms['displacementMap'].value = heightmap;
														// tileMesh.material.uniforms['displacementBias'].value = -32768;
														// tileMesh.material.uniforms['displacementScale'].value = 1.0;
														// tileMesh.material.uniforms['displacementMap'].value = heightmap;
														// tileMesh.material.uniforms['displacementScale'].value = 100;
														//
														// tileMesh.material.displacementMap = heightmap;
														// tileMesh.material.displacementScale = 100;
														//

														// if (zoom_ === this.zoom && atile === tileRef.x && btile === tileRef.y) {
														// 	// set camera z shift = altitude of origin point at (long, lat)
														// 	let zShift = layer.getZShift(
														// 		tileMesh.geometry.vertices,
														// 		this.controls.getLongitude(),
														// 		this.controls.getLatitude(),
														// 		tileRef);
														// 	this.setZShift(zShift);
														// }
														// self.logger.debug('tileMesh: ' + JSON.stringify(tileMesh), tileId);
                          },
                          lod_
                        );
                      }
										}
										)(
												tileSupport, tileMesh,
												{z: zoom_, x: atile % modulus, y: btile % modulus},
												tileShift,
                        Math.max(0, zoom_ - 15)
											);
                  }
                  break;
                case 'tile':
									let tileId = {
										z: zoom_,
										x: atile % modulus,
										y: btile % modulus
									};
                  self.logger.debug('Process tile texture: ' + zoom_ + '/' + atile + '/' + btile, tileId);
/*
                  tileMesh.material = new THREE.MeshBasicMaterial({
                    transparent: ((self.layers[layerId].opacity !== 1)),
                    opacity: self.layers[layerId].opacity
                  });
*/
									tileMesh.material = new THREE.MeshBasicMaterial();

									// tileMesh.material = new THREE.MeshPhongMaterial();

									// let shader = THREE.ShaderLib['phong'];
									// // console.log('fragmentShader:', shader.fragmentShader);
									// let uniforms = THREE.UniformsUtils.clone(shader.uniforms);
									// let defines = {
									// 	'USE_MAP': ''
									// };
									//
									// tileMesh.material = new THREE.ShaderMaterial({
									// 	name: 'TerrainShader',
									// 	defines: defines,
									// 	uniforms: uniforms,
									// 	fragmentShader: shader.fragmentShader,
									// 	vertexShader: shader.vertexShader,
									// 	fog: false,
									// 	lights: true
									// });

									// let terrainShader = THREE.ShaderTerrain['terrain'];
									// let uniformsTerrain = THREE.UniformsUtils.clone(terrainShader.uniforms);
									//
									// tileMesh.material = new THREE.ShaderMaterial({
									// 	uniforms: uniformsTerrain,
									// 	vertexShader: terrainShader.vertexShader,
									// 	fragmentShader: terrainShader.fragmentShader,
									// 	lights: true,
									// 	fog: false
									// });

                  this.render();
									let parentZoom = Math.max(self.ZOOM_MIN, zoomMin - 1);
									let parentTileId = {
										z: parentZoom,
										x: Math.floor((atile % modulus) / Math.pow(2, (zoom_ - parentZoom))),
										y: Math.floor((btile % modulus) / Math.pow(2, (zoom_ - parentZoom)))
									};
/*
                  let modulus_ = (parentZoom > 0) ? Math.pow(2, parentZoom) : 0;
                  let parentXtile = Math.floor(self.xtile / Math.pow(2, (zoomMax - parentZoom))) % modulus_;
                  let parentYtile = Math.floor(self.ytile / Math.pow(2, (zoomMax - parentZoom))) % modulus_;
*/
									self.logger.trace('parentLoad():' + parentZoom);
									self.logger.trace('parentTileId():' + parentTileId);
                  // tileLoader.parentLoad(
                  //   parentTileId,
                  //   {z: zoom_, x: atile % modulus, y: btile % modulus},
                  //   layer.getLocalUrl(parentTileId),
                  //   layer.getUrl(parentTileId),
                  //   texture => {
                  //     self.logger.debug('parent texture received.', tileId);
                  //     tileMesh.material.map = texture;
                  //     tileMesh.material.needsUpdate = true;
                  //     self.render();
                  //   }
                  // );
/*
                  tileLoader.preload(
                    {z: zoom_, x: atile % modulus, y: btile % modulus},
                    layer,
                    (texture) => {
                      self.logger.debug('texture: ' + JSON.stringify(texture));
                      tileMesh.material.map = texture;
                      tileMesh.material.needsUpdate = true;
                      self.render();
                    }
                  );
*/
                  ((tileMesh, tileId, layer) => {
                    let zoom = tileId.z;
                    let xtile = tileId.x;
                    let ytile = tileId.y;
                    let tilePath = zoom + '/' + xtile + '/' + ytile;
										self.logger.debug('tilePath: ' + JSON.stringify(tilePath), tileId);
										let tileId_ = {
											z: zoom,
											x: ((zoom > 0) ? (xtile % Math.pow(2, zoom)) : 0),
											y: ((zoom > 0) ? (ytile % Math.pow(2, zoom)) : 0)
										};
                    tileLoader.load(
											tileId_,
											layer.getLocalUrl(tileId_),
											layer.getUrl(tileId_),
											texture => {
												// // tileMesh.material.uniforms['tNormal'].texture = detailTexture;
												// tileMesh.material.uniforms['uNormalScale'].value = 1;
												// tileMesh.material.uniforms['tDiffuse1'].texture = texture;
												// tileMesh.material.uniforms['tDetail'].texture = texture;
												// tileMesh.material.uniforms['enableDiffuse1'].value = true;
												// tileMesh.material.uniforms['enableDiffuse2'].value = true;
												// tileMesh.material.uniforms['enableSpecular'].value = true;
												// // diffuse is based on the light reflection
												// // tileMesh.material.uniforms['uDiffuseColor'].value.setHex(0xcccccc);
												// // tileMesh.material.uniforms['uSpecularColor'].value.setHex(0xff0000);
												// // is the base color of the terrain
												// // tileMesh.material.uniforms['uAmbientColor'].value.setHex(0x0000cc);
												// // how shiny is the terrain
												// // tileMesh.material.uniforms['uShininess'].value = 3;
												// // handles light reflection
												// // tileMesh.material.uniforms['uRepeatOverlay'].value.set(3, 3);
												// // self.logger.debug('tileMesh.geometry: ' + JSON.stringify(tileMesh.geometry));
												// tileMesh.geometry.computeFaceNormals();
												// tileMesh.geometry.computeVertexNormals();
												// // tileMesh.geometry.computeTangents();

												// { "uniform1": { value: 1.0 }, "uniform2": { value: 2 } }
												// tileMesh.material.map = texture;
												// tileMesh.material.uniforms['map'].value = texture;
												// tileMesh.material.uniforms['map'].value = texture;

												//
												// tileMesh.material.uniforms['displacementScale'].value = 1.0;
												// tileMesh.material.uniforms['displacementBias'].value = 0;
												// tileMesh.material.uniforms['displacementMap'].value = texture;
												// tileMesh.material.displacementScale = 1000;

												tileMesh.material.map = texture;
												tileMesh.material.needsUpdate = true;
												self.render();
                      },
                      null
                    );
                    currentIds[tilePath] = {};
                  })(tileMesh, {z: zoom_, x: atile % modulus, y: btile % modulus}, this.layers[layerId]);
                  break;
                case 'building':
                  self.logger.trace('Process building: ' + zoom_ + '/' + atile + '/' + btile);
                  if (this.zoom >= 17 &&
                    zoom_ >= Math.max(this.zoom - 1, this.layers[layerId].minZoom)) {
                    const defaultColor =
                      ((13 * this.zoom) % 256) * 65536 +
                      ((53 * (atile % modulus)) % 256) * 256 +
                      ((97 * (btile % modulus)) % 256);
                    const lod = Math.max(0, zoom_ - 15);

                    ((tileSupport, tileId, lod_, defaultColor_) => {
                      // let zoom = tileId.z;
                      // let xtile = tileId.x;
                      // let ytile = tileId.y;
                      const localUrl =
                        self.layers[layerId].getLocalUrl(tileId);
// =============================================================================
                      const url = self.layers[layerId].getUrl(tileId);
                      self.loaders[layerId].load(
                        tileId,
                        localUrl,
                        url,
                        tileBuildings => {
                          tileSupport.add(tileBuildings);
                          for (let i = tileBuildings.children.length - 1; i >= 0; i--) {
                            // self.buildingObjects.push(obj.children[i]);
														let buildingMesh = tileMesh.children[i];
														for (let j = buildingMesh.children.length - 1; j >= 0; j--) {
															self.buildingObjects.push(buildingMesh.children[j]);
														}
                          }
                          self.render();
                        },
/*
                        tileData => {
                          let jsonData = JSON.parse(tileData);
                          let obj = parse(jsonData);
                          tileSupport.add(obj);
                          for (let i = obj.children.length - 1; i >= 0; i--) {
                            self.buildingObjects.push(obj.children[i]);
                          }
                          self.render();
                        },
*/
                        () => {},
                        () => {},
                        lod_,
                        defaultColor_);
                    })(tileSupport,
                      {z: zoom_, x: (atile % modulus), y: (btile % modulus)},
                      lod, defaultColor);
                  }
                  break;
                default:
                  break;
              }
            }
          }
        }
      }
    }
		// self.logger.debug('this.renderer.info: ' + JSON.stringify(this.renderer.info));
    tileLoader.cancelOtherRequests(currentIds);
  };
  setCenter(lon, lat) {
    this.controls.setCenter(lon, lat);
  };
	setZShift(zShift) {
		this.controls.setZShift(zShift);
		this.earth.position.set(0, 0, -((this.R * 1000) + zShift));
	}
  setPosition(lon, lat, alti, phi, theta) {
    this.controls.setPosition(lon, lat, alti, phi, theta);
    // this.updateScene({
    //   'lon': this.lonStamp,
    //   'lat': this.latStamp,
    //   'alti': this.altitude
    // });
  };
}
World.ZOOM_FLAT = 13;
export default World;
