// world.addLayer(new OpenEarthView.Layer.OSM(
//     "OpenStreetMap", [
//         "http://a.tile.openstreetmap.org/${z}/${x}/${y}.png",
//         "http://b.tile.openstreetmap.org/${z}/${x}/${y}.png",
//         "http://c.tile.openstreetmap.org/${z}/${x}/${y}.png"
//     ]));

class OSM {
    constructor(name, urls, options) {
        this.name = (name !== undefined) ? name : 'OpenStreetMap';
        // if (OpenEarthViewLayers.hasOwnProperty(name)) {
        //     console.err('Cannot register this already existing layer !');
        //     return;
        // }
				this.urls = urls;
        // this.urls = (urls !== undefined) ? urls : [
        //     'http://a.tile.openstreetmap.org/${z}/${x}/${y}.png',
        //     'http://b.tile.openstreetmap.org/${z}/${x}/${y}.png',
        //     'http://c.tile.openstreetmap.org/${z}/${x}/${y}.png'
        // ];
        this.type = OSM.type;
        if (options !== undefined) {
            this.minZoom = (options.minZoom !== undefined) ? options.minZoom : 18;
            this.localData = (options.localData !== undefined) ? options.localData : undefined;
        }
    }
    getName() {
        return this.name;
    }
    getLocalUrl(tileId) {
				let result;
				if (this.localData !== undefined) {
						return this.localData
								.replace('${z}', tileId.z)
								.replace('${x}', tileId.x)
								.replace('${y}', tileId.y);
				}
				return result;
    }

    getUrl(tileId) {
        let url;
        if (this.urls !== null && this.urls !== undefined) {
            let urlRandom = this.urls[
                Math.floor(Math.random() * this.urls.length)];
            url = urlRandom.replace('${z}', tileId.z);
            url = url.replace('${x}', tileId.x);
            url = url.replace('${y}', tileId.y);
        }
        return url;
    }
}
OSM.type = 'tile';
OSM.opacity = 1;
export default OSM;
