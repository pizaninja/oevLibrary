// var OpenEarthView = require("../openearthview.js");
// OpenEarthView.Terrain = require("./Terrain.js");

// this.terrains["defaultTerrain"] = new OpenEarthView.Terrain.FlatTerrain("FlatTerrain");
// // this.addTerrain("defaultTerrain", new OpenEarthView.Terrain.FlatTerrain("FlatTerrain"));
// // world.addTerrain(new OpenEarthView.Terrain.FlatTerrain("FlatTerrain"));

let THREE = require('THREE');
let toolbox = require('../toolbox.js');
let Logger = require('../logger.js');
//let R = 6378.137;
export default class {
    constructor(name, urls, options) {
        this.logger = Logger.getInstance('OpenEarthView.Layer.FlatTerrain');
        this.type = 'terrain';
        this.name = (name !== undefined) ? name : 'OpenEarthView';
        // if (OpenEarthView.Terrains.hasOwnProperty(name)) {
        //     console.err('Cannot register this already existing layer !');
        //     return;
        // }
        // OpenEarthView.Terrains[name] = [];
    }
    getName() {
        return this.name;
    }
    getUrl(tileId) {
        return undefined;
    }

		preProcess(tileSupport, tileMesh, tileId, tileShift) {
			let trapeze = toolbox.getTrapeze(tileId);
			let widthUp = trapeze.widthUp;
			let widthDown = trapeze.widthDown;
			let height = trapeze.height;
			let width = (widthUp + widthDown) / 2;
			let widthDelta = (widthDown - widthUp) / 2;

/*
             <-widthUp->
 Y           B         C
 ^   (0,0) + +---------+ +             ^
 |         |/           \|             |
 +--> X    /             \             | height
          /|             |\            |
       A +-+-------------+-+ D         -
           <----width---->
         <----widthDown---->
*/
        // Set tile terrain geometry: tileMesh.geometry
      let tileShape = new THREE.Shape();
      tileShape.moveTo(-widthDelta, -height);
      tileShape.lineTo(widthDelta, 0);
      tileShape.lineTo(width - widthDelta, 0);
      tileShape.lineTo(width + widthDelta, -height);
      tileShape.lineTo(-widthDelta, -height);

      // logger.debug('tileShape: ' + JSON.stringify(tileShape));
      tileMesh.geometry = new THREE.ShapeGeometry(tileShape);
      toolbox.singleton.assignUVs(tileMesh.geometry);

			tileSupport.position.set(
				tileShift.x * width,
				-tileShift.y * height,
				0);
		}
}
