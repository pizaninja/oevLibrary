/**
Open Earth View - viewer-threejs
The MIT License (MIT)
Copyright (c) 2016 Clément Igonet

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

* @author Clement Igonet
*/

let THREE = require('THREE');
//var Logger = require('../logger.js');
let RequestManager = require('../request/RequestManager.js');
let Logger = require('../logger.js');
let self;
class TileLoader {
    constructor() {
        self = this;
        self.logger = Logger.getInstance('OpenEarthView.Loader.TileLoader');
        // self.crossOrigin = undefined;
				let textureLoader = new THREE.TextureLoader();
				textureLoader.crossOrigin = '';
        self.requestManager = new RequestManager(
            new THREE.TextureLoader(),
            textureLoader,
            TileLoader.MAX_TILE_LOADER_REQUEST,
            'TileLoader');
    }

    // Load from any "bigger" tile
    preload(tileId, onLoaded) {
        let zoom = tileId.z;
        let xtile = tileId.x;
        let ytile = tileId.y;
//        for (let diff = 0; diff < zoom; diff++) {
        for (let diff = zoom - 2; diff <= zoom - 2; diff++) {
            let power = Math.pow(2, diff);
            let tileId = {
                z: (+zoom - diff),
                x: Math.floor(xtile / power),
                y: Math.floor(ytile / power)
            };
            this.logger.trace('Ask for texture of: ' + JSON.stringify(tileId));
            let texture_ = self.requestManager.loadCache(tileId);
            // this.logger.trace('texture_: ' + JSON.stringify(texture_));
            if (texture_ !== undefined) {
                let texture = texture_.clone();
                texture.repeat.x = 1 / power;
                texture.repeat.y = 1 / power;
                let xOffset = xtile - Math.floor(xtile / power) * power;
                let yOffset = (power - 1) - (ytile - Math.floor(ytile / power) * power);
                texture.offset.x = xOffset * texture.repeat.x;
                texture.offset.y = yOffset * texture.repeat.y;
                texture.needsUpdate = true;
                onLoaded(texture);
                return; // Escape after successful operation
            }
        }
    }

    // Load from any "bigger" tile
    parentLoad(
        parentTileId,
        tileId,
				localUrl, url, onLoad, lod) {
        self.requestManager.newRequest(
            parentTileId,
            localUrl,
            url,
            texture_ => {
                if (!self.requestManager.hasData(tileId)) {
                    let texture = texture_.clone();
                    let power = Math.pow(2, tileId.z - parentTileId.z);
                    texture.repeat.x = 1 / power;
                    texture.repeat.y = 1 / power;
                    let xOffset = tileId.x - Math.floor(tileId.x / power) * power;
                    let yOffset = (power - 1) - (tileId.y - Math.floor(tileId.y / power) * power);
                    texture.offset.x = xOffset * texture.repeat.x;
                    texture.offset.y = yOffset * texture.repeat.y;
                    texture.needsUpdate = true;
                    onLoad(texture);
                }
            }
        );
    }

   load(tileId, localUrl, url, onLoad, lod) {
        self.logger.debug('Load tile request:  ' + JSON.stringify({tileId}), tileId);
        self.requestManager.newRequest(
            tileId,
            localUrl,
            url,
            onLoad
        );
    }

    cancelOtherRequests(currentIds) {
        self.requestManager.cancelOtherRequests(currentIds);
    }
/*
    cancelOtherRequests(currentIds) {
        let textureRequests = this.textureRequests;
        let textureAliveRequests = this.textureAliveRequests;
        for (let id in textureRequests) {
            if (!currentIds.hasOwnProperty(id)) {
                delete textureRequests[id];
                this.textureRequestsCount--;
            }
        }
        for (let id in textureAliveRequests) {
            if (!currentIds.hasOwnProperty(id)) {
            }
        }
        this.loadNext();
    }
*/
}

TileLoader.MAX_TILE_LOADER_REQUEST = 10;
export default new TileLoader();
