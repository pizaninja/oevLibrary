/**
Open Earth View - viewer-threejs
The MIT License (MIT)
Copyright (c) 2016 Clément Igonet

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

* @author Clement Igonet
*/

// loadNext()
// preLoad()
// request()
// cancelOtherRequests()

/*
 _____________
|             |
|    Init     |
|_____________|
       |
       - loadNext()
       |
                             -----------------------------------
                            |                                   |
 _______                 ___V_____                              |
|       |  Ready.load() |         |     LoadOverPass.progress() |
| Ready |-----|-------->| Loading |---|-------------------------
|_______|               |_________|   |
                                      |                         _________
                                      | LoadOverPass.success() |         |
                                      '-----|----------------->| Loaded  |
                                      |                        |_________|
                                      |                         ____________
                                      | LoadOverPass.fail()    |            |
                                      '-----|----------------->| LoadFailed |
                                                               |____________|

1 - localLoad
2 - preLoad
3 - remoteLoad
*/

// OpenEarthView.TileLoader = function() {
//     this.textureLoader = new THREE.TextureLoader();
//     this.textureLoader.crossOrigin = '';
//     // this.textureLoader
// };
var THREE = require('THREE');
var Logger = require('../logger.js');
let self;
class TileLoader {
    constructor() {
        self = this;
        this.logger = Logger.getInstance('OpenEarthView.Loader.TileLoader');
        this.textureLoader = new THREE.TextureLoader();
        this.textureLoader.crossOrigin = '';
        this.textures = {};
        this.textureRequests = {};
        this.textureRequestsCount = 0;
        this.textureAliveRequests = {};
        this.textureAliveRequestsCount = 0;
    }
 
    loadNext() {
        let textures = this.textures;
        let textureRequests = this.textureRequests;
        let textureAliveRequests = this.textureAliveRequests;
        while (this.textureAliveRequestsCount < TileLoader.MAX_TEXTURE_REQUEST && this.textureRequestsCount > 0) {
            let ids = Object.keys(textureRequests);
            let id = ids[ids.length - 1];
            this.textureAliveRequestsCount = this.textureAliveRequestsCount + (textureAliveRequests.hasOwnProperty(id) ? 0 : 1);
            textureAliveRequests[id] = textureRequests[id];
            let url = textureAliveRequests[id].url;
            delete textureRequests[id];
            this.textureRequestsCount--;
            (function(url, id) {
                self.logger.debug('Asking for loading: ' + url);
                textureAliveRequests[id].request = self.textureLoader.load(
                    url,
                    function onLoad(texture) {
                        textures[id] = texture;
                        if (textureAliveRequests.hasOwnProperty(id)) {
                            self.logger.debug('Texture loaded: ' + url);
                            textureAliveRequests[id].onLoaded(texture);
                            delete textureAliveRequests[id];
                            self.textureAliveRequestsCount--;
                        }
                        self.loadNext();
                    },
                    function onProgress() {},
                    function onError() {
                        if (textureAliveRequests.hasOwnProperty(id)) {
                            delete textureAliveRequests[id];
                            self.textureAliveRequestsCount--;
                        }
                        self.loadNext();
                    }
                );
            })(url, id);
        }
    };

    // Load from a "bigger" tile
    preload(tile, onLoaded) {
        let zoom = tile.zoom;
        let xtile = tile.xtile;
        let ytile = tile.ytile;
        let textures = this.textures;
        for (let diff = 0; diff < zoom; diff++) {
            let power = Math.pow(2, diff);
            let idZoomOther = (+zoom - diff) + '/' + Math.floor(xtile / power) + '/' + Math.floor(ytile / power);
            if (textures.hasOwnProperty(idZoomOther)) {
                let tex = textures[idZoomOther].clone();
                tex.repeat.x = 1 / power;
                tex.repeat.y = 1 / power;
                let xOffset = xtile - Math.floor(xtile / power) * power;
                let yOffset = (power - 1) - (ytile - Math.floor(ytile / power) * power);
                tex.offset.x = xOffset * tex.repeat.x;
                tex.offset.y = yOffset * tex.repeat.y;
                tex.needsUpdate = true;
                onLoaded(tex);
                return;
            }
        }
    }
    request(tile, onLoaded) {
        let url = tile.url;
        let zoom = tile.zoom;
        let xtile = tile.xtile;
        let ytile = tile.ytile;
        let textures = this.textures;
        let textureRequests = this.textureRequests;
        let id = zoom + '/' + xtile + '/' + ytile;
        if (textures.hasOwnProperty(id)) {
        } else {
            this.textureRequestsCount = this.textureRequestsCount + (textureRequests.hasOwnProperty(id) ? 0 : 1);
            textureRequests[id] = {
                zoom: zoom,
                xtile: xtile,
                ytile: ytile,
                url: url,
                onLoaded: onLoaded
            };
            this.loadNext();
        }
    }

    load(tileId, localUrl, url, onLoad, lod) {
        self.requestManager.newRequest(
            tileId,
            localUrl,
            url,
            (png, tileId) => onLoad(THREE.TerrariumPngLoader.parse(
                png,
                tileId
            )
        );
    }

    cancelOtherRequests(currentIds) {
        let textureRequests = this.textureRequests;
        let textureAliveRequests = this.textureAliveRequests;
        for (let id in textureRequests) {
            if (!currentIds.hasOwnProperty(id)) {
                delete textureRequests[id];
                this.textureRequestsCount--;
            }
        }
        for (let id in textureAliveRequests) {
            if (!currentIds.hasOwnProperty(id)) {
            }
        }
        this.loadNext();
    }

}
TileLoader.MAX_TEXTURE_REQUEST = 10;
export default new TileLoader();
