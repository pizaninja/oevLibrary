// TOOLBOX //
/**
Open Earth View - viewer-threejs
The MIT License (MIT)
Copyright (c) 2016 Clément Igonet

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

// OpenEarthView.toolbox = function() {
//     this.textureLoader = new THREE.TextureLoader();
//     this.textureLoader.crossOrigin = '';
// };
// var OpenEarthView = require("./openearthview.js");

let instances = {};
//
// class Cache {
//     constructor() {
//         if (!instance) {
//             instance = this;
//         }
//
//         // to test whether we have singleton or not
//         this.time = new Date()
//
//         return instance;
//     }
// }

class Logger {
    constructor(className) {
        if (!instances.hasOwnProperty(className)) {
//            console.log('Create logger for:', className) ;
            instances[className] = this;
        }
        this.className = className;
        this.logLevel = Logger.DefaultLogLevel;
				this.tileId = undefined;
/*
        this.geoTiles = {};
        this.materials = {};
        this.materialQueue = [];
*/
        return instances[className];
    }
/*
    Example:
    # Define expected log level
    ```
    car logger = new Logger()
    logger.add(this.constructor.name, Logger.Level['INFO']);
    ```
    # Write a message to logger
    ## Quick solution
    ```
    logger.log(this.constructor.name, Logger.Level['ERROR'], 'Error message');
    ```
    ## Class dedicated solution
    Or define a shortcut log function:
    ```
    MyClass.log(logLevel, message) {
        logger.log(this.constructor.name, logLevel, message)
    }
    ```
    And then log message:
    ```
    this.log(Logger.ERROR, 'Error message');
    ```
*/
    setLogLevel(logLevel) {
        this.logLevel = logLevel;
//        console.log(this.className, '->', Logger.logLevelStr[logLevel]) ;
    }

    setTileId(tileId) {
				this.tileId = tileId;
		}

    getLogLevel() {
        return this.logLevel;
    }

    log(logLevel, message) {
//        console.log(this.format(this.className, logLevel) + message);
        if (logLevel <= this.logLevel) {
//            console.log(this.className, '-', Logger.toString(logLevel), ':', message);
            console.log(this.format(this.className, logLevel) + message);
        }
    }

    error(message) { this.log(Logger.Level.ERROR, message); }
    warning(message) { this.log(Logger.Level.WARNING, message); }
    info(message) { this.log(Logger.Level.INFO, message); }
    debug(message, tileId) {
				if (this.tileId === undefined || tileId === undefined) {
						this.log(Logger.Level.DEBUG, message);
				} else if (this.tileId !== undefined && tileId !== undefined && JSON.stringify(tileId) === JSON.stringify(this.tileId)) {
						this.log(Logger.Level.DEBUG, JSON.stringify(tileId) + ' - ' + message);
				}
		}
    trace(message, tileId) {
				if (this.tileId === undefined || tileId === undefined) {
						this.log(Logger.Level.TRACE, message);
				} else if (this.tileId !== undefined && tileId !== undefined && JSON.stringify(tileId) === JSON.stringify(this.tileId)) {
						this.log(Logger.Level.TRACE, JSON.stringify(tileId) + ' - ' + message);
				}
		}

    format(className, logLevel) {
//        return className + ' - ' + Logger.toString(logLevel) + ' :;
        let className_ = this.className;
        if (this.className.length > 38) {
            className_ = className.substring(0, 5) +
                '...' +
                className.substring(Math.max(className.length - 30, 0), className.length);
        }
        return className_ + '/' + Logger.toString(logLevel) + ': ';
    }
    static toString(logLevel) {
        return Logger.logLevelStr[logLevel];
    }
    static getInstance(className) {
        let result;
        if (instances.hasOwnProperty(className)) {
            result = instances[className];
        } else {
            result = new Logger(className);
        }
//        console.log('Ask for instance:', className, 'at logLevel:', result.getLogLevel());
        return result;
    }
}

//Logger.singleton = new Logger();
Logger.logLevelStr = ['ERROR', 'WARNING', 'INFO', 'DEBUG', 'TRACE'];
Logger.Level = {
    'ERROR': 0,
    'WARNING': 1,
    'INFO': 2,
    'DEBUG': 3,
    'TRACE': 4
};
Logger.ERROR = Logger.Level['ERROR'];
Logger.WARNING = Logger.Level['WARNING'];
Logger.INFO = Logger.Level['INFO'];
Logger.DEBUG = Logger.Level['DEBUG'];
Logger.TRACE = Logger.Level['TRACE'];
Logger.DefaultLogLevel = Logger.INFO;
export default Logger;
