var THREE = require('THREE');

THREE.OEVShaderTerrain = {
		uniforms: {
				'map': { type: 't', value: null },
				'displacementMap': { type: 't', value: null },
				'displacementBias': { value: 0.0 },
				'displacementScale': { value: 1 }
		},
		fragmentShader:	`
			uniform sampler2D map;
			varying vec2 vUv;
			void main() {
			  gl_FragColor = texture2D(map, vUv);
			}
		`,
		vertexShader: `
			uniform sampler2D displacementMap;
			uniform float displacementScale;
			uniform float displacementBias;

			varying float vAmount;
			varying vec2 vUv;
			varying vec3 vViewPosition;
			void main() {
				// gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
				// vec3 dv = texture2D(displacementMap, vUv).rgb;
				// float df1 = (dv.r * 256.0) + dv.g + (dv.b / 256.0);
				// float df2 = df1 + displacementBias;
				// float df3 = displacementScale * df2;

				// vec3 dv = texture2D(displacementMap, uv).xyz;
				// float df = displacementScale * dv.x + displacementBias;
				// vec3 displacedPosition = normal * df + position;

				// float scaleAmt = 1.0;
				// vUV = uv;

				vUv = uv;
				vec4 bumpData = texture2D(displacementMap, uv);
				// vAmount will be within [0.0, 300.0] with water level being 100.0
				vAmount = dot(bumpData, vec4(255.0, 1.0, 1.0/255.0, 0)) - displacementBias;
				vec3 newPosition = position + normal * displacementScale * vAmount;
				gl_Position = projectionMatrix * modelViewMatrix * vec4(newPosition, 1.0);

				// vec3 displacedPosition = vec3(position.x, position.y, normal.z * df3 + position.z);
				// vec4 worldPosition = modelMatrix * vec4( displacedPosition, 1.0 );
				// vec4 mvPosition = modelViewMatrix * vec4( displacedPosition, 1.0 );
				// gl_Position = projectionMatrix * mvPosition;
				// vViewPosition = -mvPosition.xyz;
			}
		`
};

export default THREE.OEVShaderTerrain;
