// TOOLBOX //
/**
Open Earth View - viewer-threejs
The MIT License (MIT)
Copyright (c) 2016 Clément Igonet

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

// OpenEarthView.toolbox = function() {
//     this.textureLoader = new THREE.TextureLoader();
//     this.textureLoader.crossOrigin = '';
// };
// var OpenEarthView = require("./openearthview.js");
var THREE = require('THREE');

let instance = null;
//
// class Cache {
//     constructor() {
//         if (!instance) {
//             instance = this;
//         }
//
//         // to test whether we have singleton or not
//         this.time = new Date()
//
//         return instance;
//     }
// }
let Logger = require('./logger.js');

class Toolbox {
    constructor() {
        if (!instance) {
            instance = this;
        }
        this.geoTiles = {};
        this.geoTileQueue = [];
        this.materials = {};
        this.materialQueue = [];
        return instance;
    }

    assignUVs(geometry) {

        geometry.computeBoundingBox();

        let max = geometry.boundingBox.max;
        let min = geometry.boundingBox.min;

        let offset = new THREE.Vector2(0 - min.x, 0 - min.y);
        let range = new THREE.Vector2(max.x - min.x, max.y - min.y);

        geometry.faceVertexUvs[0] = [];
        let faces = geometry.faces;

        for (let i = 0; i < geometry.faces.length; i++) {
            let v1 = geometry.vertices[faces[i].a];
            let v2 = geometry.vertices[faces[i].b];
            let v3 = geometry.vertices[faces[i].c];

            geometry.faceVertexUvs[0].push([
                new THREE.Vector2((v1.x + offset.x) / range.x, (v1.y + offset.y) / range.y),
                new THREE.Vector2((v2.x + offset.x) / range.x, (v2.y + offset.y) / range.y),
                new THREE.Vector2((v3.x + offset.x) / range.x, (v3.y + offset.y) / range.y)
            ]);
        }
        geometry.uvsNeedUpdate = true;
    }
    getTileMesh(r, zoom, ytile, power) {

        let id = 'tile_' + zoom + '_' + ytile + '_' + power;

        if (!(this.geoTiles.hasOwnProperty(id))) {
            this.geoTiles[id] = new THREE.Geometry();
            let myGeometry = this.geoTiles[id];

            this.geoTileQueue.push(id);
            if (this.geoTileQueue.length > this.MAX_TILEMESH) {
                delete this.geoTiles[this.geoTileQueue.shift()];
            }
            /*************************
             *            ^ Y         *
             *            |           *
             *            |           *
             *            |           *
             *            -------> X  *
             *           /            *
             *          /             *
             *         / Z            *
             *************************/
            /***************************
             *       B        C         *
             *                          *
             *                          *
             *                          *
             *      A          D        *
             ***************************/
            let lonStart = Toolbox.tile2long(0, zoom);
            let latStart = Toolbox.tile2lat(ytile, zoom);
            let lonEnd = Toolbox.tile2long(1, zoom);
            let latEnd = Toolbox.tile2lat(ytile + 1, zoom);
            let factor = Math.pow(2, power);
            let lonStep = (lonEnd - lonStart) / factor;
            let latStep = (latEnd - latStart) / factor;

            for (let u = 0; u < factor; u++) {
                for (let v = 0; v < factor; v++) {

                    let lon1 = lonStart + lonStep * u;
                    let lat1 = latStart + latStep * v;
                    let lon2 = lonStart + lonStep * (u + 1);
                    let lat2 = latStart + latStep * (v + 1);

                    let rUp = r * 1000 * Math.cos(lat1 * Math.PI / 180);
                    let rDown = r * 1000 * Math.cos(lat2 * Math.PI / 180);

                    let Ax = rDown * Math.sin(lon1 * Math.PI / 180);
                    let Ay = r * 1000 * Math.sin(lat2 * Math.PI / 180);
                    let Az = rDown * Math.cos(lon1 * Math.PI / 180);

                    let Bx = rUp * Math.sin(lon1 * Math.PI / 180);
                    let By = r * 1000 * Math.sin(lat1 * Math.PI / 180);
                    let Bz = rUp * Math.cos(lon1 * Math.PI / 180);

                    let Cx = rUp * Math.sin(lon2 * Math.PI / 180);
                    let Cy = r * 1000 * Math.sin(lat1 * Math.PI / 180);
                    let Cz = rUp * Math.cos(lon2 * Math.PI / 180);

                    let Dx = rDown * Math.sin(lon2 * Math.PI / 180);
                    let Dy = r * 1000 * Math.sin(lat2 * Math.PI / 180);
                    let Dz = rDown * Math.cos(lon2 * Math.PI / 180);

                    myGeometry.vertices.push(
                        new THREE.Vector3(Ax, Ay, Az),
                        new THREE.Vector3(Bx, By, Bz),
                        new THREE.Vector3(Cx, Cy, Cz),
                        new THREE.Vector3(Dx, Dy, Dz)
                    );

                    let iStep = (factor - v - 1) + u * factor;

                    myGeometry.faces.push(new THREE.Face3(
                        4 * iStep,
                        4 * iStep + 2,
                        4 * iStep + 1));
                    myGeometry.faces.push(new THREE.Face3(
                        4 * iStep,
                        4 * iStep + 3,
                        4 * iStep + 2));

                    myGeometry.faceVertexUvs[0].push([
                        new THREE.Vector2((0.0 + u) / factor, (0.0 + v) / factor),
                        new THREE.Vector2((1.0 + u) / factor, (1.0 + v) / factor),
                        new THREE.Vector2((0.0 + u) / factor, (1.0 + v) / factor)
                    ]);
                    myGeometry.faceVertexUvs[0].push([
                        new THREE.Vector2((0.0 + u) / factor, (0.0 + v) / factor),
                        new THREE.Vector2((1.0 + u) / factor, (0.0 + v) / factor),
                        new THREE.Vector2((1.0 + u) / factor, (1.0 + v) / factor)
                    ]);
                }
            }
            myGeometry.uvsNeedUpdate = true;
        }
        return this.geoTiles[id];
    }

    // static getSearchParameters() {
    //     let prmstr = window.location.search.substr(1);
    //     return prmstr !== null && prmstr !== '' ? transformToAssocArray(prmstr) : {};
    // }
		static long2tile(lon, zoom) {
        return (Math.floor((lon + 180) / 360 * Math.pow(2, zoom)));
    }
    static lat2tile(lat, zoom) {
        return (Math.floor((1 - Math.log(Math.tan(lat * Math.PI / 180) + 1 / Math.cos(lat * Math.PI / 180)) / Math.PI) / 2 * Math.pow(2, zoom)));
    }
		static long2tileFloat(lon, zoom) {
        return (Math.floor((lon + 180) / 360 * Math.pow(2, zoom)));
    }
    static lat2tileFloat(lat, zoom) {
        return (Math.floor((1 - Math.log(Math.tan(lat * Math.PI / 180) + 1 / Math.cos(lat * Math.PI / 180)) / Math.PI) / 2 * Math.pow(2, zoom)));
    }
    static tile2long(x, z) {
        return ((x / Math.pow(2, z) * 360 - 180) + 540) % 360 - 180;
    }
    static tile2lat(y, z) {
        var n = Math.PI - 2 * Math.PI * y / Math.pow(2, z);
        return (180 / Math.PI * Math.atan(0.5 * (Math.exp(n) - Math.exp(-n))));
    }
    static measure(lat1, lon1, lat2, lon2) { // generally used geo measurement function
        // var R = 6378.137; // Radius of earth in KM
        let dLat = (lat2 - lat1) * Math.PI / 180;
        let dLon = (lon2 - lon1) * Math.PI / 180;
        let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2);
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        let d = Toolbox.R * c;
        return d * 1000; // meters
    }
    static lonOffsetMeter2lon(lon, lat, x) {
        return x / (Toolbox.R * Math.cos(lat)) + lon;
    }
    static latOffsetMeter2lat(lat, y) {
        return (y / Toolbox.R) + lat;
    }

    static makeTextSprite(message, parameters) {
        if (parameters === undefined) parameters = {};
        let fontface = parameters.hasOwnProperty('fontface') ?
            parameters['fontface'] : 'Arial';
        let fontsize = parameters.hasOwnProperty('fontsize') ?
            parameters['fontsize'] : 108;
        let borderThickness = parameters.hasOwnProperty('borderThickness') ?
            parameters['borderThickness'] : 4;
        let borderColor = parameters.hasOwnProperty('borderColor') ?
            parameters['borderColor'] : {
                r: 0,
                g: 0,
                b: 0,
                a: 1.0
            };
        let backgroundColor = parameters.hasOwnProperty('backgroundColor') ?
            parameters['backgroundColor'] : {
                r: 255,
                g: 255,
                b: 255,
                a: 1.0
            };

        // let spriteAlignment = THREE.SpriteAlignment.topLeft;

        let canvas = document.createElement('canvas');
        // let canvas = document.getElementById('osmworld');
        console.log('canvas:', canvas);
        let context = canvas.getContext('2d');
        context.font = 'Bold ' + fontsize + 'px ' + fontface;

        // get size data (height depends only on font size)
        let metrics = context.measureText(message);
        let textWidth = metrics.width;

        // background color
        context.fillStyle = 'rgba(' + backgroundColor.r + ',' + backgroundColor.g + ',' + backgroundColor.b + ',' + backgroundColor.a + ')';
        // border color
        context.strokeStyle = 'rgba(' + borderColor.r + ',' + borderColor.g + ',' + borderColor.b + ',' + borderColor.a + ')';

        context.lineWidth = borderThickness;
        Toolbox.roundRect(context, borderThickness / 2 + 140 - textWidth / 2, borderThickness / 2, textWidth + borderThickness, fontsize * 1.4 + borderThickness, 6);
        // Toolbox.roundRect(context, borderThickness / 2 + textWidth / 2, borderThickness / 2, textWidth + borderThickness, fontsize * 1.4 + borderThickness, 6);

        // 1.4 is extra height factor for text below baseline: g,j,p,q.

        // text color
        context.fillStyle = 'rgba(0, 0, 0, 1.0)';

        context.fillText(message, borderThickness + 140 - textWidth / 2, fontsize + borderThickness);
        // context.fillText(message, 0, fontsize + borderThickness);

        // canvas contents will be used for a texture
        let texture = new THREE.Texture(canvas);
        texture.needsUpdate = true;

        let spriteMaterial = new THREE.SpriteMaterial({
            map: texture,
            useScreenCoordinates: false
                // alignment: spriteAlignment
        });
        let sprite = new THREE.Sprite(spriteMaterial);
        sprite.scale.set(100, 50, 1.0);
        return sprite;
    }
    static roundRect(ctx, x, y, w, h, r) {
        ctx.beginPath();
        ctx.moveTo(x + r, y);
        ctx.lineTo(x + w - r, y);
        ctx.quadraticCurveTo(x + w, y, x + w, y + r);
        ctx.lineTo(x + w, y + h - r);
        ctx.quadraticCurveTo(x + w, y + h, x + w - r, y + h);
        ctx.lineTo(x + r, y + h);
        ctx.quadraticCurveTo(x, y + h, x, y + h - r);
        ctx.lineTo(x, y + r);
        ctx.quadraticCurveTo(x, y, x + r, y);
        ctx.closePath();
        ctx.fill();
        ctx.stroke();
    }

		static getHeightmapData(img, tileId, factor, scale, shift) {
				let logger = Logger.getInstance('OpenEarthView.Toolbox.getHeightmapData');
				// logger.debug('getHeightData()');
				// logger.debug('tileId: ' + JSON.stringify(tileId));
				// logger.debug('factor: ' + factor);
				let power = Math.pow(2, factor);
				let canvas = document.createElement('canvas');
				let context;
				let size;
				let data;
				// let j;
				let imgd;
				let pix;
				canvas.width = img.width;
				canvas.height = img.height;
				context = canvas.getContext('2d');
				// logger.debug('img.height: ' + img.height);
				size = img.height * img.width / (power * power);
				// logger.debug('size: ' + size);

				data = new Float32Array(size);
				context.drawImage(img, 0, 0);

				let width = img.width / power;
				let height = img.height / power;
				// logger.debug('tileId.x: ' + tileId.x);
				// logger.debug('power: ' + power);
				// logger.debug('(tileId.x % power): ' + (tileId.x % power));
				// logger.debug('img.width: ' + img.width);
				// logger.debug('(tileId.x % power) * img.width: ' + ((tileId.x % power) * img.width));
				// logger.debug('((tileId.x % power) * img.width) / power: ' + (((tileId.x % power) * img.width) / power));
				let x0 = ((tileId.x % power) * img.width) / power;
				let y0 = ((tileId.y % power) * img.height) / power;
				// let x0 = img.width * (tileId.x - (Math.floor(tileId.x / power) * power)) / Math.pow(2, tileId.z);
				// let y0 = img.height * (tileId.y - (Math.floor(tileId.y / power) * power)) / Math.pow(2, tileId.z);

				// logger.debug('(x0, y0): ' + '(' + x0 + ', ' + y0 + ')', tileId);
				// console.log('(x0, y0): ' + '(' + x0 + ', ' + y0 + ')');
				logger.debug('(h, w): ' + '(' + height + ', ' + width + ')', tileId);
				imgd = context.getImageData(x0, y0, width, height);
				pix = imgd.data;
				// logger.debug('imgd.data: ' + JSON.stringify(imgd.data), tileId);
				for (let i = 0, n = pix.length, j = 0; i < n; i += 4) {
						let all = (pix[i] * 256 + pix[i + 1] + pix[i + 2] / 256) + shift;
						// logger.debug('all: ' + all, tileId);
						data[j++] = all * scale;
				}

				// imgd = context.getImageData(0, 0, img.width, img.height);
				// pix = imgd.data;
				// for (let y = y0; y < y0 + height; y++) {
				// 	for (let x = x0; x < x0 + width; x++) {
				// 		let all01 = toolbox.getPix(pix, x, y, img.width, img.height);
				// 		let all02 = toolbox.getPix(pix, x - 1, y, img.width, img.height);
				// 		let all03 = toolbox.getPix(pix, x, y, img.width, img.height);
				// 		let all04 = toolbox.getPix(pix, x, y, img.width, img.height);
				// 		(pix[img.width * y * 4 + x * 4] * 256 +
				// 			pix[img.width * y * 4 + x * 4 + 1] +
				// 			pix[img.width * y * 4 + x * 4 + 2] / 256) + shift;
				// 			let all = (all01 / 4) + (all02 / 4) + (all03 / 4) + (all04 / 4)
				// 	}
				// }

				// logger.debug('data: ' + JSON.stringify(data), tileId);
				// logger.debug('data.length: ' + data.length);
				return data;
		}

		// static getHeightData(img, scale, shift) {
		// 		let canvas = document.createElement('canvas');
		// 		let context;
		// 		let size;
		// 		let data;
		// 		let j;
		// 		let imgd;
		// 		let pix;
		// 		canvas.width = img.width;
		// 		canvas.height = img.height;
		// 		context = canvas.getContext('2d');
		// 		size = img.height * img.width;
		// 		data = new Float32Array(size);
		//
		// 		context.drawImage(img, 0, 0);
		//
		// 		imgd = context.getImageData(0, 0, img.width, img.height);
		// 		pix = imgd.data;
		//
		// 		j = 0;
		// 		for (let i = 0, n = pix.length; i < n; i += 4) {
		// 				let all = pix[i] * 256 + pix[i + 1] + pix[i + 2] / 256 + shift;
		// 				data[j++] = all * scale;
		// 		}
		// 		return data;
		// }

    static boundingBoxMesh(object, color) {
		let indices = new Uint16Array([ 0, 1, 1, 2, 2, 3, 3, 0, 4, 5, 5, 6, 6, 7, 7, 4, 0, 4, 1, 5, 2, 6, 3, 7 ]);
		let positions = new Float32Array(8 * 3);
		let geometry = new THREE.BufferGeometry();
		geometry.setIndex(new THREE.BufferAttribute(indices, 1));
		geometry.addAttribute('position', new THREE.BufferAttribute(positions, 3));

		let lineSegments = new THREE.LineSegments(
			geometry,
			new THREE.LineBasicMaterial({ color: color })
		);

		let box = new THREE.Box3();
		box.setFromObject(object);
		let min = box.min;
		let max = box.max;
		/*
		  5____4
		1/___0/|
		| 6__|_7
		2/___3/

		0: max.x, max.y, max.z
		1: min.x, max.y, max.z
		2: min.x, min.y, max.z
		3: max.x, min.y, max.z
		4: max.x, max.y, min.z
		5: min.x, max.y, min.z
		6: min.x, min.y, min.z
		7: max.x, min.y, min.z
		*/
		let position = lineSegments.geometry.attributes.position;
		let array = position.array;
		array[0] = max.x; array[1] = max.y; array[2] = max.z;
		array[3] = min.x; array[4] = max.y; array[5] = max.z;
		array[6] = min.x; array[7] = min.y; array[8] = max.z;
		array[9] = max.x; array[10] = min.y; array[11] = max.z;
		array[12] = max.x; array[13] = max.y; array[14] = min.z;
		array[15] = min.x; array[16] = max.y; array[17] = min.z;
		array[18] = min.x; array[19] = min.y; array[20] = min.z;
		array[21] = max.x; array[22] = min.y; array[23] = min.z;
		position.needsUpdate = true;
		lineSegments.geometry.computeBoundingSphere();

		return lineSegments;
	}

	// getPix(pix, x, y, height, width, shift) {
	// 	return
	// 		pix[width * y * 4 + x * 4] * 256 +
	// 		pix[width * y * 4 + x * 4 + 1] +
	// 		pix[width * y * 4 + x * 4 + 2] / 256) + shift;
	// }
	static getSubTexture(parentTexture, tileId, factor) {
		let texture = parentTexture.clone();
		let power = Math.pow(2, tileId.z - factor);
		texture.repeat.x = 1 / power;
		texture.repeat.y = 1 / power;
		let xOffset = tileId.x - Math.floor(tileId.x / power) * power;
		let yOffset = (power - 1) - (tileId.y - Math.floor(tileId.y / power) * power);
		texture.offset.x = xOffset * texture.repeat.x;
		texture.offset.y = yOffset * texture.repeat.y;
		texture.needsUpdate = true;
		return texture;
	}

  static getTrapeze(tileId) {
		let lon1 = Toolbox.tile2long(tileId.x, tileId.z);
		let lon2 = Toolbox.tile2long(tileId.x + 1, tileId.z);
		let lat1 = Toolbox.tile2lat(tileId.y, tileId.z);
		let lat2 = Toolbox.tile2lat(tileId.y + 1, tileId.z);
		let widthUp = Toolbox.measure(lat1, lon1, lat1, lon2);
		let widthDown = Toolbox.measure(lat2, lon1, lat2, lon2);
		let height = Toolbox.measure(lat1, 0, lat2, 0);
		return {
			widthUp: widthUp,
			widthDown: widthDown,
			height: height
		};
	}

    static originAxes(radius, height) {
        // static originAxes() {
        let origin = new THREE.Object3D();

        //radiusTop, radiusBottom, height, segmentsRadius, segmentsHeight, openEnded
        let ge3 = new THREE.CylinderGeometry(radius, radius, height, 4, 1);
        let axeXMesh = new THREE.Mesh(ge3,
            new THREE.MeshBasicMaterial({
                color: Math.random() * 0xffffff
//                opacity: 0.7
            })
        );
        axeXMesh.rotation.set(0, 0, Math.PI / 2);
        axeXMesh.position.setX(500);
        origin.add(axeXMesh);
        let axeYMesh = new THREE.Mesh(ge3,
            new THREE.MeshBasicMaterial({
                color: Math.random() * 0xffffff,
                opacity: 0.7
            })
        );
        axeYMesh.position.setY(500);
        origin.add(axeYMesh);
        let axeZMesh = new THREE.Mesh(ge3,
            new THREE.MeshBasicMaterial({
                color: Math.random() * 0xffffff,
                opacity: 0.7
            })
        );
        axeZMesh.rotation.set(Math.PI / 2, 0, 0);
        axeZMesh.position.setZ(500);
        origin.add(axeZMesh);
        return origin;
    }
};
Toolbox.R = 6378.137;
Toolbox.singleton = new Toolbox();
export default Toolbox;
