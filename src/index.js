// export default class OpenEarthView {
//     constructor() {
//         this._name = 'OpenEarthView';
//     }
//     get name() {
//         return this._name;
//     }
// }

var THREE = require('THREE');
// var Stats = require('Stats');
THREE.EarthControls = require('./controls/EarthControls.js');

export default {
    Logger: require('./logger.js'),
    World: require('./world.js'),
    toolbox: require('./toolbox.js'),
    RequestManager: require('./request/RequestManager.js'),
    Layer: {
        // OverpassBuilding: require('./layers/OverpassBuildingLayer.js'),
        OSM: require('./layer/OsmLayer.js'),
        FlatTerrain: require('./layer/FlatTerrain.js')
    }
    // EarthControls: require('./controls/EarthControls_function.js')
    // EarthControls: require('./controls/EarthControls.js')
};
// OpenEarthView.TileLoader = require("./loaders/TileLoader.js");
